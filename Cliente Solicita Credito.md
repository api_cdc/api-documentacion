# Cliente solicita crédito

## Descripción

Determina si el cliente ha solicitado un crédito dentro del rango de tiempo especificado por el otorgante.

## Consultar si el cliente ha solicitado créditos

## URL

>`http://algo.com`

## Http Method: POST

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|**persona**|Objeto|Si|Los datos generales de la persona a consultar|
|└─**primerNombre**|String|Si|Primer Nombre de la persona|
|└─**segundoNombre**|String|No|Segundo Nombre de la persona|
|└─**apellidoPaterno**|String|Si|Apellido paterno de la persona|
|└─**apellidoMaterno**|String|Si|Apellido Materno de la persona|
|└─**apellidoAdicional**|String|No|Apellido adicional de la persona|
|└─**fechaNacimiento**|String|Si|Fecha de nacimiento de la persona en formato Y-m-d|
|└─**curp**|String|No|CURP de la persona|
|└─**rfc**|String|No|RFC de la persona|
|└─**domicilio**|Objeto|Si|Datos del domicilio de la persona a consultar|
|└────**domicilio**|String|Si|Calle y numero del domicilio|
|└────**colonia**|String|Si|Colonia del domicilio|
|└────**ciudad**|String|No|Ciudad del domicilio|
|└────**municipio**|String|Si|Municipio del domicilio|
|└────**estado**|String|Si|Estado del domicilio|
|└────**pais**|String|No|País del domicilio|
|└────**cp**|Int|No|Código postal del domicilio|
|└─**periodo**|String|Si|Periodo de tiempo dentro del cual se considera que el cliente se encuentra solictando crédito, la cadena debe estar compuesta por la cantidad y la unidad de tiempo que se desea substraer a partir de la fecha actual. Unidades: d -> día, w -> semana, m -> mes|
|└─**folio**|String|No|Folio de consulta del otorogante|

## Ejemplo de Solicitud

```bash
curl -X POST \
https://algo.com \
-H 'Authorization: {{TOKEN}}' \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'x-api-key: {{API_KEY}}' \
-d '{
  "persona": {
    "primerNombre": "{{primerNombre}}",
    "apellidoPaterno": "{{apellidoPaterno}}",
    "apellidoMaterno": "{{apellidoMaterno}}",
    "fechaNacimiento": "{{fechaNacimiento}}",
    "domicilio": {
      "domicilio": "{{domicilio}}",
      "colonia": "{{colonia}}",
      "municipio": "{{municipio}}",
      "estado": "{{estado}}",
      "cp": {{codigoPostal}}
    }
  },
  "periodo": "{{periodo}}"
}'
```
**Nota:** Se reemplazan las "{{value}}" por el valor correspondiente

## Propiedades de la respuesta

|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|**solicitaCredito**|Bool|Verdadero si se encuentra una consulta dentro del rango especificado, de lo contrario, falso|
|**fechaUltimaConsulta**|String|La fecha en que fue realizada la cosulta por algún otorgante|

## Ejemplo de respuseta

```bash
{
  "response": {
    "isSuccess": true,
    "errors": [],
    "data": {
      "solicitaCredito": true,
      "fechaUltimaConsulta": "2018-05-15"
    }
  }
}
```