﻿# Consulta Expediente

## Descripción

Consulta el expediente de una persona y genera un folio de consulta mediante la información de la persona, así obteniendo toda la información crediticia de la persona, si esta existe.

El servicio cuenta con dos opciones de solicitud una mediante una simple petición **Rest** y otra mediante una consulta **[Graphql](http://graphql.org/learn/)**.

## Solicitud simple rest

Se realiza la consulta mediante una solicitud simple **Rest**.

## URL
> `https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/expediente`

## HTTP Method: POST

## Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|folioOtorgante|String|Si|**Folio interno del otorgante**
|claveOtorgante|String|Si|**Clave del otorgante**
|ficoScore|Boolean|Si|Si es verdadero trae el ficoScore
|formatoFecha|String|Si|**Formato de fecha** que recibe y regresa referencia [click aquí](https://bitbucket.org/api_cdc/api-documentacion/wiki/Date%20Format)
|**persona**|Objeto|Si|Información de la persona
|└─primerNombre|String|Si|El **primer nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─segundoNombre|String|No|El **segundo nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─apellidoPaterno|String|Si|El **apellido paterno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoMaterno|String|No|El **apellido materno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoAdicional|String|No|Se debe utilizar para reportar el **apellido de casada**.
|└─fechaNacimiento|String|Si|**Fecha de nacimiento**
|└─rfc|String|No|Reglas:<br>Las primeras 4 posiciones deben ser alfabéticas.<br>1. 5 y 6 deben contener un número entre 00 y 99.<br>2. 7 y 8 deben contener un número entre 01 a 12.<br>3. 9 y 10 deben contener un número entre 01 a 31.  <br>4. 11 -13 homoclave (opcional).<br> Los RFC's de personas extranjeras deben cumplir con las características arriba mencionadas|
|└─direccion|String|Si|La **dirección de la persona** incluyendo nombre de la calle, número exterior y/o interior. Deben considerarse avenida, cerrada, manzana, lote, edificio, departamento etc.
|└─colonia|String|No|La **colonia** a la cual pertenece la dirección de la persona
|└─ciudad|String|No|La **ciudad** a la cual pertenece la dirección de la persona
|└─codigoPostal|String|Si|El **código postal** reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado.  <br>En caso de una longitud de 4 dígitos completar con cero a la izquierda (08564)
|└─municipio|String|No|La **delegación** a la que pertenece la dirección de la persona
|└─estado|String|Si|La abreviatura correspondiente
|└─pais|String|Si|Debe contener el país donde se encuentra el domicilio del Acreditado
|└─curp|String|No|Reglas: <br>1. 4 posiciones deben ser alfabéticas.  <br>2. 5 y 6 posiciones deben contener un número entre 00 y 99 (año).<br>3. 7 y 8 posiciones deben contener un número entre 01 y 12 (mes).<br>4. 9 y 10 posiciones deben contener un número entre 01 y 31 (día).  <br>5. 11-16 posiciones deben ser alfabéticas.  <br>6. 17 numérico (homoclave).  <br>7. 18 numérico (Dígito Verificador).


#### Ejemplo Request - JSON

```bash
curl -X POST \
  https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/expediente \
  -H 'Authorization: {{TOKEN}}' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: {{API_KEY}}' \
  -d '{
	 "folioOtorgante":{{folioOtorgante}},
	 "claveOtorgante":"{{claveOtorgante}}",
	 "ficoScore":{{ficoScore}},
     "formatoFecha":"yyyy-MM-dd",
	 "persona":{
	 	"apellidoAdicional":  "{{apellidoAdicional}}" ,
		"apellidoMaterno":  "{{apellidoMaterno}}" ,
		"apellidoPaterno":  "{{apellidoPaterno}}" ,
		"ciudad":  "{{ciudad}}" ,
		"codigoPostal":  "{{codigoPostal}}",  
		"colonia":  "{{colonia}}" ,
		"curp":  "{{curp}}" ,
		"municipio":  "{{municipio}}" ,
		"direccion":  "{{direccion}}" ,
		"estado":  "{{estado}}" ,
		"fechaNacimiento":  "{{fechaNacimiento}}" ,
		"pais":  "{{pais}}" ,
		"primerNombre":  "{{primerNombre}}" ,
		"rfc":  "{{rfc}}" ,
		"segundoNombre":  "{{segundoNombre}}" 
	 }

}'
```
  **Nota:** Se reemplazan las "{{value}}" por el valor correspondiente

## Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|**claveFolio**|PeticionClaveFolio|Clave de la Persona
|└─clavePersona|int|Clave de la persona
|└─folioConsulta|int|Folio de consulta
|└─claveOtorgante|String|ClaveOtorgante
|**consultas** |Lista Consulta |Lista de consulta
|└─fechaConsulta|String|Fecha en que Círculo de Crédito registró la consulta. Formato de fecha AAA-MM-DD.
|└─nombreOtorgante|String|Se presentará el Nombre de la Institución Otorgante que reporta la cuenta, el dato es asignado por Círculo de Crédito.
|└─claveOtorogante|String|Dirección de otorgante
|└─direccionOtorgante|String|Dirección de otorgante
|└─telefonoOtorgante|String|Número telefonico de Otorgante
|└─tipoCredito|String|Tipo de prestamo que se solicito
|└─importeCredito|String|Monto solicitado
|└─tipoResponsabilidad|String|Tipo de responsabilidad respecto al crédito solicitado
|└─servicios|String|
|└─idDomicilio|Integer|
|**creditos** |List Crédito |Lista de créditos
|└─fechaActualizacion|Date|Fecha (AAAAMMDD) correspondiente al periodo que se está reportando.
|└─registroImpugnado|int|Si el crédito fue impugnado (tiene alguna investigación por alguna controversia ya sea por parte del usuario o el cliente)
|└─claveOtorgante|String|Reportar la. Clave asignada por Círculo de Crédito a 10 posiciones.
|└─nombreOtorgante|String|Reportar el nombre del Otorgante que reporta la cuenta. Cuando se trate de un reporte especial o sea una cuenta propia de la institución al momento de la consulta, este nombre aparecerá en la sección de detalle de créditos en el reporte de Crédito. El dato correspondiente a este elemento es asignado por Círculo de Crédito.
|└─cuentaActual|String|Es el número de cuenta del crédito, este número es asignado por el Usuario.<br>Todas las cuentas reportadas en una base deben ser únicas por contrato.
|└─tipoResponsabilidad|String|Indica la responsabilidad que tiene el Cliente con el crédito otorgado.
|└─tipoCuenta|String|Indica el tipo de cuenta que el Usuarios dio al Cliente. Ver Tabla: Tipo de Cuenta
|└─tipoCredito|String|El producto que se otorgó al Cliente.
|└─claveUnidadMonetaria|String|Los valores posibles son los siguientes:<br>MX = Pesos <br>US = Dólares <br>UD = Unidades de inversión
|└─valorActivoValuacion|Long|Aplica en pagos fijos (F) o hipotecario (H), el dato se refiere al valor total del activo para propósitos de evaluación o recuperación. Es el valor monetario de la garantía.
|└─numeroPagos|Long|Es el total de pagos determinado en la apertura del crédito. Si el tipo de cuenta es Pagos fijos (F) o Hipoteca (H), este elemento se hace Requerido con un valor mayor a cero, para otro Tipo de Cuenta es opcional
|└─frecuenciaPagos|String|Reportar la perioricidad con que el Cliente debe realizar sus pagos. Ver Tabla: Frecuencia de Pagos
|└─montoPagar|Long|Es la cantidad que el Cliente debe pagar de acuerdo con la frecuencia de pagos establecida. La cantidad a pagar debe ser un número entero mayor o igual a cero. (Monto acordado de cada parcialidad). Ver Tabla: Monto a Pagar y Saldo Actual
|└─fechaAperturaCuenta|Date|Reportar la Fecha en la que se otorgó el crédito. <br>La fecha no debe ser mayor a 100 años
|└─fechaUltimoPago|Date|Es la fecha más reciente en la que el Cliente efectuó un pago. Ver Tabla: Fecha de Último Pago y Fecha de Última compra
|└─fechaUltimaCompra|Date|Fecha más reciente en que el Clientes efectuó una compra o disposición de un crédito. Si el tipo de cuenta es Fijo o Hipotecario, la Fecha de Última compra será igual a la Fecha de Apertura de la Cuenta. Ver Tabla: Fecha de Último Pago y Fecha de Última compra
|└─fechaCierraCuenta|Date|Fecha en la que se liquidó o cerró un crédito.
|└─fechaReporte|Date|
|└─ultimaFechaSaldoCero|Date|La ultima vez que la tarjeta de crédito quedo en ceros
|└─garantia|String|Reportar una descripción de la garantía utilizada para asegurar el crédito otorgado.
|└─creditoMaximo|Long|Contiene el máximo importe de crédito utilizado por el Cliente. El valor debe ser mayor o igual a cero. Aplicable a Tipo de cuenta (F) Pagos Fijos, (H) Hipoteca y (R) Revolvente Ver Tabla: Crédito Máximo.
|└─saldoActual|Long|La cantidad debe ser un número entero. Es el importe total del adeudo que tiene el Clientes a la fecha de reporte incluyendo intereses. En caso de no tener saldo este elemento debe reportarse en cero (0) para cerrar la cuenta.
|└─limiteCredito|Long|El límite de crédito que el Usuario extiende al Cliente. <br>Este campo es obligatorio para créditos revolventes.
|└─saldoVencido|Long|Cantidad generada a la fecha de reporte por atraso en pagos. Deberá ser un número entero y positivo. Ver Tabla: Saldo Actual y Saldo Vencido
|└─numeroPagosVencidos|int|Número de pagos vencidos
|└─pagoActual|String|Forma de pagos
|└─historicoPagos|String|
|└─fechaRecienteHistoricoPagos|Date|
|└─fechaAntiguaHistoricoPagos|Date|
|└─clavePrevencion|String|Se utiliza para reportar situaciones especiales que presenta la cuenta (por ejemplo para deducir impuestos de un prestamo que el banco considera que ya no se pagará
|└─totalPagosReportados|Long|Es el total de pagos realizados por el Cliente a la fecha que se está reportando. Es númerico de 3
|└─peorAtraso|int|La mayor cantidad de pagos incumplidos en el historico del cliente
|└─fechaPeorAtraso|Date|La Fecha (AAAAMMDD) en la que el cliente tuvo su peor atraso
|└─saldoVencPeorAtraso|Long|Saldo vencido del peor atraso
|└─montoUltimoPago|Double|Monto último pago
|└─idDomicilio|Long|
|└─servicios|Long|
|**domicilios**|Lista Domicilio|Lista de domicilios
|└─tipoDomicilio|String|Tipo Domicilio
|└─coloniaPoblacion|String|La colonia a la cual pertenece la dirección de la persona
|└─delegacionMunicipio|String|La delegación a la pertenece la dirección de la persona
|└─ciudad|String|La Ciudad a la cual pertenece la dirección de la persona
|└─estado|String|La abreviatura correspondiente**.Ver en Tabla: Estados de la República
|└─cp|String|El código postal reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado.<br> En caso de una longitud de 4 dígitos completar con cero a la izquierda (08564).
|└─fechaResidencia|Date|Fecha de la residencia
|└─tipoAsentamiento|String|Tipo de Asentamiento
|└─direccion|String|Direccion
|└─numExterior|Integer|numero exterior de la vivienda
|└─numInterior|Integer|numero interior de la vivienda
|└─fechaRegistroDomicilio|Date|Fecha de Registro
|└─numeorTelefono|String|Número de telefono del domicilio
|└─tipoAltaDomicilio|int|Tipo de alta de domicilio
|└─numeroOtorgantesCarga|int|Número de otorgante de carga
|└─numeroOtorgantesConsulta|int|Número de otorgante de consulta
|└─idDomicilio|int|
|**empleos**|List<Empleo>|Lista de empleos
|└─nombreEmpresa|String|Debe reportarse el Nombre o Razón Social de la empresa que emplea al Cliente. <br>Cuando el consumidor sea trabajador independiente, no está asociado a una empresa o no cuenta con un trabajo podrá reportarse uno de los siguientes posibles valores:<br>•Trabajador Independiente <br>•Estudiante<br>•Labores de Hogar<br>•Jubilado<br>•Desempleado<br>•Exempleado"
|└─direccion|String|La dirección del cliente incluyendo nombre de la calle, número exterior y/o interior. Deben considerarse avenida, cerrada, manzana, lote, edificio, departamento etc.
|└─coloniaPoblacion|String|La colonia a la cual pertenece la dirección del cliente
|└─delegacionMunicipio|String|La delegación a la pertenece la dirección del cliente
|└─ciudad|String|La Ciudad a la cual pertenece la dirección del cliente
|└─estado|String|La abreviatura correspondiente**.Ver en Tabla: Estados de la República
|└─cp|String|El código postal reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado. <br>En caso de una longitud de 4 dígitos completar con apóstrofe y cero a la izquierda ('08564). Ver Tabla: Rangos Códigos Postales"
|└─numeroTelefono|String|"Reportar el número telefónico de empleo del Acreditado.<br>Cada caracter debe ser un número de 0-9, si se ingresa cualquier otro caracter el registro será rechazado.<br>En caso de no contar con algun empleo, colocar el mismo dato del teléfono del acreditado."
|└─extension|String|Si se cuenta con la información reportar la extensión telefónica de la persona.
|└─fax|String|Mismos criterios de validación que en el Elemento Número Telefónico
|└─puesto|String|Reportar el título o posición de empleo del Acreditado, si se tiene disponible
|└─fechaContratacion|Date|
|└─claveMoneda|String|Es el tipo de moneda que se le paga al Acreditado en su empleo. <br>Valores para este Elemento: <br>MX = Pesos Mexicanos <br>US = Dólares <br>UD = Unidades de Inversión. <br>En caso de Reportar Salario Mensual este elemento se hace requerido.
|└─salarioMensual|String|Reportar el Ingreso Mensual del Acreditado. <br>En caso de colocar Clave de Moneda este elemento es requerido. <br>Se aceptan números enteros y sin caracteres especiales.
|└─fechaUltimoDiaEmpleo|Date|Debe reportarse la fecha del último día de trabajo en esa empresa.
|└─fechaVerificacionEmpleo|Date|Fecha en que el otorgante verificó los datos proporcionados por el Acreditado
|**persona** |Persona |Persona
|└─apellidoPaterno|String|El Apellido Paterno completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoMaterno|String|El Apellido Materno completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─fechaNacimiento|Date|Fecha de Nacimiento
|└─sexo|String|Sexo
|└─rfc|String|Reglas: <br>Las primeras 4 posiciones deben ser alfabéticas. <br>5 y 6 deben contener un número entre 00 y 99. <br>7 y 8 deben contener un número entre 01 a 12. <br>9 y 10 deben contener un número entre 01 a 31. <br>11 -13 homoclave (opcional). <br>Los RFC's de personas extranjeras deben cumplir con las características arriba mencionadas
|└─numeroDependientes|Integer|El número de dependientes económicos del Cliente
|└─nacionalidad|String|Debe contener el país donde se encuentra el domicilio del Acreditado. Ver Anexos A: Tabla de Nacionalidades
|└─residencia|Integer|
|└─estadoCivil|String|Estado Civil
|└─curp|String|Reglas: <br>1- 4 posiciones deben ser alfabéticas. <br>2- 5 y 6 posiciones deben contener un número entre 00 y 99 (año). <br>3- 7 y 8 posiciones deben contener un número entre 01 y 12 (mes). <br>4- 9 y 10 posiciones deben contener un número entre 01 y 31 (día).<br>5- 11-16 posiciones deben ser alfabéticas. <br>6- 17 numérico (homoclave).<br>7- 18 numérico (Dígito Verificador).
|└─nombre|String|Nombre de la persona
|└─fechaDefuncion|Date|Fecha de Defuncion
|**ficoScore**|Objeto|Fico Score
|└─**score**|int| Score
|└─**exclusionCode**|int| Exclusion Code
|└─**erroCode**|int|
|└─**razones**|array| 
|└──clave|String|Clave
|└──descripcion|String|Descripción

## Ejemplo Response - JSON

```bash

{
	"response":{
		"isSuccess":true,
		"success":{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":{
			"claveFolio":{
				"clavePersona":40212897,
				"folioConsulta":18582443,
                "claveOtorgante":"00001"
			},
			"consultas":[
				{
					"fechaConsulta":"2018-04-09",
					"nombreOtorgante":"CONSUPRESTA",
					"direccionOtorgante":"PASEO DE LA REFORMA",
                    "claveOtorogante":"0001",
					"tipoCredito":"F",
					"importeCredito":"1",
					"tipoResponsabilidad":"F"
				},
				{
					"fechaRegistro":"2018-04-06",
					"nombreOtorgante":"CONSUPRESTA",
					"direccionOtorgante":"PASEO DE LA REFORMA",
					"cveContrato":"F",
					"claveUnidadMonetaria":"MX",
					"importeContrato":"1",
					"cveTipoResp":"F"
				}
			],
			"creditos":[  
				{
	                "registroImpugnado": 0,
                    "claveOtorgante": "000001",
                    "nombreOtorgante": "REPORTE DE CREDITO ESPECIAL",
                    "cuentaActual": "000171840000330044",
                    "tipoResponsabilidad": "I",
                    "tipoCuenta": "F",
                    "tipoCredito": "LC",
                    "claveUnidadMonetaria": "MX",
                    "valorActivoValuacion": 1413,
                    "numeroPagos": 26,
                    "frecuenciaPagos": "S",
                    "montoPagar": 102,
                    "fechaAperturaCuenta": "2012-11-29",
                    "fechaUltimoPago": "2013-05-16",
                    "fechaUltimaCompra": "2012-11-29",
                    "fechaReporte": "2017-10-30",
                    "creditoMaximo": 2656,
                    "saldoActual": 310,
                    "limiteCredito": 0,
                    "saldoVencido": 0,
                    "numeroPagosVencidos": 1,
                    "historicoPagos": " V V V01 V V V V V V V V V V V V V V V V V V V V V",
                    "totalPagosReportados": 0,
                    "peorAtraso": 1,
                    "fechaPeorAtraso": "2013-04-25",
                    "saldoVencidoPeorAtraso": 102
	            }
            ],	
			"domicilios":[
				{
					"direccion": "AV ACUEDUCTO MZ111 LT4B",
                    "coloniaPoblacion": "LA PALMA",
                    "delegacionMunicipio": "ECATEPEC DE MORELOS",
                    "ciudad": null,
                    "estado": "MEX",
                    "cp": 55507,
                    "fechaResidencia": "2013-04-07",
                    "numeroTelefono": null,
                    "tipoDomicilio": "O",
                    "tipoAsentamiento": "0",
                    "fechaRegistroDomicilio": "2013-04-07",
                    "tipoAltaDomicilio": null,
                    "numeroOtorgantesCarga": null,
                    "numeroOtorgantesConsulta": null,
                    "idDomicilio": 119315811
				}
			],
			"empleos":[  {
                    "folio": "18580501",
                    "cvepersona": 0,
                    "generico": 0,
                    "nombre": null,
                    "direccion": null,
                    "colonia": null,
                    "delegMunic": null,
                    "ciudad": null,
                    "estado": null,
                    "codigo": "92100",
                    "numTelefono": null,
                    "numExtension": null,
                    "numFax": null,
                    "puesto": "MANTENIMIENTO",
                    "fechaAlta": -2177452800000,
                    "unidadMonetaria": null,
                    "salarioMensual": null,
                    "fechaUltDia": 1247616000000,
                    "fechaVerif": 1247616000000,
                    "usuario": "0"
                }],
			"persona":{
				"apellidoPaterno": "RAMIREZ",
                "apellidoMaterno": "SANCHEZ",
                "rfc": "RASM820130",
                "fechaNacimiento": "1982-01-30",
                "nombres": "MARTIN",
                "nacionalidad": "MX",
                "estadoCivil": "L",
                "sexo": "M",
                "numeroDependientes": 3
				}
		}
	}
}
```


##  Solicitud mediante Graphql

La solicitud via graphql se realiza mediante la sintaxis de [graphql](http://graphql.org/learn/)  con los mismos parametros de entrada que en la consuta normal, la salida depende de los filtros de la consulta generada.

## URL
> `https://nobv442rmh.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/expediente/graphql`

## HTTP Method: POST

## Ejemplo Request - GRAPHQL

```bash
query expediente($peticion: PeticionExpediente!) {
  expediente(peticion: $peticion) {
    persona {
      nombres
      apellidoPaterno
      apellidoMaterno
      estadoCivil
    },
    domicilios {
      direccion
    }
    empleos {
      puesto
    }
  }
}

Parametros del query:
{
  "peticion": {
    "folioOtorgante": "89091992",
    "claveOtorgante": "9001",
    "match": {
    	"primerNombre": "MARTIN",
      "segundoNombre": "",
      "apellidoPaterno": "RAMIREZ",
      "apellidoMaterno": "SANCHEZ",
      "rfc": "RASM820130",
      "curp": "",
      "fechaNacimiento": "1982-01-30",
      "direccion": "AV ACUEDUCTO MZ111 LT4B",
      "colonia": "LA PALMA",
      "codigoPostal": "55507",
      "municipio": "ECATEPEC DE MORELOS",
      "estado": "MEX",
      "pais": ""
    }
  }
}

https://nobv442rmh.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/expediente/graphql
```