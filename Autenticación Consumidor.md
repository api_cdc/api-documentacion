# Autenticación Consumidor

## Descripción

Asegurar que quien va a recibir el crédito es quien dice ser, mediante un conjunto de preguntas y respuestas reguladas por Banxico.

El servicio cuenta con dos etapas una de generación de opciones de otorgantes (entre los cuales puede estar quien haya otorgado crédito a la persona o no) y otro la evaluación de estas.

## Generar Otorgantes

La generación de otorgantes es el primer paso para la autenticacíon de la persona.

## URL

> `https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/autenticacion/consumidor/otorgantes`

## HTTP Method: POST

## Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|**folioOtorgante**|String|Si|Folio otorgante que el otorgante conoce y todos los que solicitan match deben conocerlo
|**persona**|Objeto|Si|Datos de la persona que solicita la autenticación
|└─primerNombre|String|Si|El **primer nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─segundoNombre|String|No|El **segundo nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─apellidoPaterno|String|Si|El **apellido paterno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoMaterno|String|No|El **apellido materno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoAdicional|String|No|Se debe utilizar para reportar el **apellido de casada**.
|└─fechaNacimiento|String|Si|**Fecha de nacimiento** en el formato yyyy-MM-dd.
|└─rfc|String|No|Reglas:  <br>1\.Las primeras 4 posiciones deben ser alfabéticas. <br>2\. 5 y 6 deben contener un número entre 00 y 99. <br>3\. 7 y 8 deben contener un número entre 01 a 12. <br>4\. 9 y 10 deben contener un número entre 01 a 31. <br>5\. 11 -13 homoclave (opcional). <br>6\.Los RFC's de personas extranjeras deben cumplir con las características arriba mencionadas|
|└─direccion|String|Si|La **dirección de la persona** incluyendo nombre de la calle, número exterior y/o interior. Deben considerarse avenida, cerrada, manzana, lote, edificio, departamento etc.
|└─colonia|String|No|La **colonia** a la cual pertenece la dirección de la persona
|└─ciudad|String|No|La **ciudad** a la cual pertenece la dirección de la persona
|└─codigoPostal|String|Si|El **código postal** reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado.  En caso de una longitud de 4 dígitos completar con cero a la izquierda (08564)
|└─municipio|String|No|La **delegación** a la pertenece la dirección de la persona
|└─estado|String|Si|La [abreviatura](https://bitbucket.org/api_cdc/api-documentacion/wiki/Clave%20Estados%20de%20la%20Republica) correspondiente
|└─pais|String|Si|Debe contener el país donde se encuentra el domicilio del Acreditado
|└─curp|String|No|Reglas: <br>1\. 4 posiciones deben ser alfabéticas. <br>2\. 5 y 6 posiciones deben contener un número entre 00 y 99 (año). <br>3\. 7 y 8 posiciones deben contener un número entre 01 y 12 (mes). <br>4\. 9 y 10 posiciones deben contener un número entre 01 y 31 (día). <br>5\. 11-16 posiciones deben ser alfabéticas. <br>6\. 17 numérico (homoclave). <br>7\. 18 numérico (Dígito Verificador).


## Ejemplo Request - JSON

```bash
curl -X POST \
  https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/autenticacion/consumidor/otorgantes \
  -H 'Authorization: {{TOKEN}}' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: {{API_KEY}}' \
  -d '{
	"folioOtorgante":"{{folio}}",
  "persona":{
    "cveOtorgante":"{{cveOtorgante}}",
    "primerNombre":"{{primerNombre}}",
    "segundoNombre":"{{segundoNombre}}",
    "apellidoPaterno":"{{apellPaterno}}",
    "apellidoMaterno":"{{apellMaterno}}",
    "apellidoAdicional":"{{apellAdicional}}",
    "fechaNacimiento":"{{lsFecha}}",
    "direccion":"{{direccion}}",
    "colonia":"{{colonia}}",
    "ciudad":"{{ciudad}}",
    "codigoPostal":"{{codigo}}",
    "municipio":"{{delegMunic}}",
    "estado":"{{estado}}",
    "rfc":"{{rfc}}",
    "pais":"{{pais}}",
    "curp": "{{curp}}"
    }
}'
```
**Nota:** Se reemplazan las "{{value}}" por el valor correspondiente

## Response Properties

|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|**otorgantesTarjeta**|Lista de Objetos|Las opciones para otrogante de Tarjeta de Crédito|
|└─claveOtorgante|String|Clave del otorgante|
|└─nombreOtorgante|String|Nombre del Otorgante|
|**otorgantesHipotecarioAutomotriz**|Lista de Objetos|Las opciones para otorgante de Crédito Hipotecario / Automotriz|
|└─claveOtorgante|String|Clave del otorgante|
|└─nombreOtorgante|String|Nombre del Otorgante|
|claveAutenticacion|int|La clave de autenticación generada|

## Ejemplo Response - JSON

```bash
{
	"response":{
		"isSuccess":true,
		"success":{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":{
			"otorgantesTarjeta": [{
            	"claveOtorgante": "0001",
                "nombreOtorgante": "BANCO AZTECA"
            }, {
            	"claveOtorgante": "0002",
                "nombreOtorgante": "COPPEL"
            }, {
            	"claveOtorgante": "0003",
                "nombreOtorgante": "SEARS"
            }, {
            	"claveOtorgante": "0004",
                "nombreOtorgante": "ELEKTRA"
            }],
            "otorgantesHipotecarioAutomotriz": [{
            	"claveOtorgante": "0001",
                "nombreOtorgante": "BANCO AZTECA"
            }, {
            	"claveOtorgante": "0002",
                "nombreOtorgante": "COPPEL"
            }, {
            	"claveOtorgante": "0004",
                "nombreOtorgante": "ELEKTRA"
            }, {
            	"claveOtorgante": "0005",
                "nombreOtorgante": "FAMSA"
            }]
			"claveAutenticacion": 97158
		}
	}
}	
```


## Evaluar Autenticación

La evaluación de autenticación es el segundo paso para la autenticación de la persona.

## URL

> `https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/autenticacion/consumidor`

## HTTP Method: POST

## Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|claveAutenticacion|int|Si|La clave de autenticación generada en el paso anterior|
|tarjetaCredito|Objeto|No|Datos de la tarjeta de crédito para autenticar|
|└─claveOtorgante|String|Si|La Clave del otorgante de la tarjeta de crédito|
|└─numeroTarjeta|String|Si|El número de la tarjeta de crédito|
|└─limiteCreditoMesAnterior|String|Si|El límite de crédito de la tarjeta el mes anterior|
|creditoHA|Objeto|No|Datos del crédito Hipotecario / Automotriz para autenticar|
|└─claveOtorgante|String|Si|La clave del otorogante del crédito Hipotecario / Automotriz|
|└─numeroContrato|String|Si|El número de contrato del crédito Hipotecario / Automotriz

## Ejemplo Request - CURL

```bash
curl -X POST \
  https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/autenticacion/consumidor \
  -H 'Authorization: {{TOKEN}}' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: {{API_KEY}}' \
  -d '{
	"claveAutenticacion":{{claveAutenticacion}},
	"tarjetaCredito":{
		"claveOtorgante":"{{TC.claveOtorgante}}",
		"numeroTarjeta":"{{TC.numeroTarjeta}}",
		"limiteCreditoMesAnterior":"{{limiteCreditoMesAnterior}}"
		},
	"creditoHA":{
		"claveOtorgante":"{{AH.claveOtorgante}}",
		"numeroContrato":"{{AH.numeroContrato}}"
	
		}
}'
```
**Nota:** Se reemplazan las "{{value}}" por el valor correspondiente

## Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|autenticacion|Boolean|Regresa verdadero si este autentica
|numAutenticacion|String|Número de autenticación
|intentos|int|Numero de intentos de la solicitud anteriores

## Ejemplo Response - JSON

```bash
{
	"response":{
		"isSuccess":true,"success":{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":{
			"autentica": false,
            "intentosDisponibles": 2
		}
	}
}
```