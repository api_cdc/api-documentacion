# Obtener Token - Oauth2
## Introducción

Para el consumo de todos los servicios es necesario la presentación de un token de acceso 
que permita al cliente de la api ser autorizado para el consumo de los servicios, el modelo
de negociación de token sigue las especificaciones [Oauth2](https://tools.ietf.org/html/rfc6749) y [JWT](https://tools.ietf.org/html/rfc7519).

---

# Oauth2
Es un protocolo de autorización que define los flujos de negociación tokens de acceso, el  formato de token no es del alcance de la especificación y queda a consideración de la implementación, pero en nuestro caso, el token que se obtiene en respuesta es del tipo JWT.

## Participantes del flujo
Los principales actores en un flujo de autenticación son:

**Cliente:** Aplicación o api de terceros que necesita consumir información del recurso.

**Dueño del Recurso:** Usuario que tiene permisos de acceder a información de sus recursos.

**Servidor de Autorización:** Servidor encargado de generar tokens de acceso y refresco
después de una autenticación correcta de un dueño del recurso. 

**Servidor de Recursos:** Servidor que contiene y protege sus recursos a través de un token de acceso.

## Negociación de credenciales
Oauth2 está pensado para integrar múltiples clientes de distintas características, permitiendo seleccionar alguno de los flujos que se apegue más a sus necesidades. Los principales tipos de negociación de credenciales son:

**Contraseña:**
Este tipo de flujo es muy simple de usar pero debe usarse solo cuando hay un alto nivel de confianza con la aplicación cliente. Este flujo requiere acceso a las credenciales de un usuario, pero como beneficio limita la exposición de las credenciales ya que una vez intercambiadas por un token de acceso y refresco, no es necesario que se usen las credenciales del cliente mientras dure el tiempo de vida de los tokens.

**Credenciales de Cliente:** 
Este tipo de flujo es usado cuando se requiere otorgar acceso limitado a 
un recurso protegido a un cliente que actúa en nombre de sí mismo o de un 
tercero sin identificarse directamente como el dueño del recurso.
Un ejemplo muy comun de este flujo es el uso de una llave de un api de mapas.

**Código de Autorizacion:** 
Este flujo de negociación de token de acceso pone al servidor de autorización como intermediario entre el cliente y el dueño del recurso, sin exponer en ningún momento las llaves del dueño de un recurso a la aplicación cliente.

El cliente obtiene el código de autorización por medio de una redirección de regreso al cliente una vez que el servidor de autorización realizó la verificación del mismo.

Un ejemplo muy común de este flujo son los formularios de login de acceso que soportan autorización por redes sociales.

**Token de Refresco**
Este flujo permite generar tokens de acceso apartir de un token de refresco, estos tokens 
tienen un tiempo de vida mas largo al del token de acceso.

El tiempo de vida para el token de acceso de esta api es de **60 minutos**.

El tiempo de vida para el token de refresco de esta api es de **7 días**.

---

# Forma de Uso

El proceso para ejecutar un microservicio, por ejemplo para el tipo de credenciales (**Credenciales de Cliente**)
debe seguir el siguiente flujo:


1) Solicitar un token de acceso
```curl
curl -X POST {{HOST}}/{{AMBIENTE}}/auth/realms/circulo-credito-api/protocol/openid-connect/token \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'x-api-key: {{LLAVE_USO}}' \
  -d 'grant_type={{TIPO_CREDENCIALES}}&username={{USUARIO}}&password={{PASSWORD}}&client_id={{CLIENTE}}&client_secret={{LLAVE_CLIENTE}}'
```
Haciendo la sustitucion de las configuraciones de pruebas:
```curl
curl -X POST \
  https://3ukk53u1hf.execute-api.us-west-1.amazonaws.com/beta-i/auth/realms/circulo-credito-api/protocol/openid-connect/token \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'x-api-key: UqBaZg3Xaf5SMhQ2TBOU825Rj1J851L59nD6FqH5' \
  -d 'grant_type=password&username=circulo-user&password=circulo-pass&client_id=circulo-credito-api&client_secret=e3f178a9-93ad-4acc-b7f4-5979165e239e'
```

2) Recuperar y almacenar el valor del token de acceso y refresco
```json
{
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWckVoaFZrMHJzNGJwX0wzNi1IXy1RcFZwQWRoYTFNM3M2SDBFQUVHSl9NIn0.eyJqdGkiOiIwNGIwYzc5ZS00Yjk5LTRiZmUtYmJlYS1mMWRmM2I1MzU3OGIiLCJleHAiOjE1MjY5OTQyMzcsIm5iZiI6MCwiaWF0IjoxNTI2OTkzOTM3LCJpc3MiOiJodHRwczovL2VpczBjeGpjc2EuZXhlY3V0ZS1hcGkudXMtd2VzdC0xLmFtYXpvbmF3cy5jb20vYXV0aC9yZWFsbXMvY2lyY3Vsby1jcmVkaXRvLWFwaSIsImF1ZCI6ImNpcmN1bG8tY3JlZGl0by1hcGkiLCJzdWIiOiJhY2Q3Y2MxNS0wZGM2LTRhZTItYTUxNy02OGVkY2JmOWNlOWIiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJjaXJjdWxvLWNyZWRpdG8tYXBpIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiZTI1ZDg4MmYtN2M1NC00NDRhLWIwY2UtZjVhYmU3NTE2ODA1IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6W10sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJhY3R1YWxpemFjaW9uX2luZm9ybWFjaW9uX2NyZWRpdGljaWEiLCJjb25zdWx0YV9leHBlZGllbnRlIiwiYXV0ZW50aWNhY2lvbl9vdG9yZ2FudGUiXX0sInJlc291cmNlX2FjY2VzcyI6e30sIm5hbWUiOiJjaXJjdWxvdXNlciBjaXJjdWxvdXNlciIsInByZWZlcnJlZF91c2VybmFtZSI6ImNpcmN1bG8tdXNlciIsImdpdmVuX25hbWUiOiJjaXJjdWxvdXNlciIsImZhbWlseV9uYW1lIjoiY2lyY3Vsb3VzZXIiLCJlbWFpbCI6ImNpcmN1bG91c2VyQGNpcmN1bG91c2VyIn0.WfquNlBUFLyzlXZfWe3qqhjKHxlHwnCjbOAvQ-M7DP_QpENmTncpqdLbjaCEOlAsia4eiLxSc2apduOBEZFJrwzOJwlJBeTyQTGzCwaQu17tdZE9ZyXQbodj82MZYQf4AgEgkvCIzyKJ6VxwOvtGoKMOo074_xbsNISFhjCi4NV_M8RcqpHwyzN6x43S-bP2in_bBvDzo1Dl9WSU-fLjm3XVFpBHFNeR0gPpYWkQrviKtWam-BoTzmmu6gEl7Tu_G7XYS76Ibo380wf3lSb-S_1gKddztS0w-9x6-dIY9OxkQMiZ6G_lcLq_N0ivXjsyIgTYbByCi1jB-9VyVQWkNg",
    "expires_in": 300,
    "refresh_expires_in": 1800,
    "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWckVoaFZrMHJzNGJwX0wzNi1IXy1RcFZwQWRoYTFNM3M2SDBFQUVHSl9NIn0.eyJqdGkiOiIxY2E1Y2M2NC1iYjY1LTQ5NGQtYjE4MC00YTBhN2I2NjMwOTUiLCJleHAiOjE1MjY5OTU3MzcsIm5iZiI6MCwiaWF0IjoxNTI2OTkzOTM3LCJpc3MiOiJodHRwczovL2VpczBjeGpjc2EuZXhlY3V0ZS1hcGkudXMtd2VzdC0xLmFtYXpvbmF3cy5jb20vYXV0aC9yZWFsbXMvY2lyY3Vsby1jcmVkaXRvLWFwaSIsImF1ZCI6ImNpcmN1bG8tY3JlZGl0by1hcGkiLCJzdWIiOiJhY2Q3Y2MxNS0wZGM2LTRhZTItYTUxNy02OGVkY2JmOWNlOWIiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiY2lyY3Vsby1jcmVkaXRvLWFwaSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImUyNWQ4ODJmLTdjNTQtNDQ0YS1iMGNlLWY1YWJlNzUxNjgwNSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJhY3R1YWxpemFjaW9uX2luZm9ybWFjaW9uX2NyZWRpdGljaWEiLCJjb25zdWx0YV9leHBlZGllbnRlIiwiYXV0ZW50aWNhY2lvbl9vdG9yZ2FudGUiXX0sInJlc291cmNlX2FjY2VzcyI6e319.gw48h4Y8mghf8y77XzGeKDZzmwnKkc9HVDY087uoOr4wnFp0jFcFBx6FXsfLty9siPjQodCsTtDGO0-Uqd0UV0i6XJzpE1TYjm3-OQh7OodRO_fKo7X4USjQpZwfTkBR2n9TkgQrWDW8CUpZzQffdcyfx6GQ9wxYf28XpQsYms9gr1rrZ8QLlUoYaS6HuF4hNB23VQqLthcSZgDBlGyp-IwIS15bv_SgxpJRTd7XEHska8vtT2vmXt8mLVVCf3WHG8sKglXEQaMY0otEBALPGUdMWEAU53oHhzeF4rdGDSd0O2WcNa7zWC9ZxO3KIwCJOP-kHQTSV-ViQhyYMVmeMA",
    "token_type": "bearer",
    "not-before-policy": 1523657693,
    "session_state": "e25d882f-7c54-444a-b0ce-f5abe7516805"
}
```

3) Usar el token de acceso en el header **"Authorization"** con cada invocacion de la api,
es posible validar de antemano si el token aun no ha expirado usando un [api para JWT](https://auth0.com/docs/api-auth/tutorials/verify-access-token)

```curl
curl -X POST \
  https://3ukk53u1hf.execute-api.us-west-1.amazonaws.com/beta-i/{{MICROSERVICIO_A_USAR}} \
  -H 'Accept: application/json' \
  -H 'Authorization: eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWckVoaFZrMHJzNGJwX0wzNi1IXy1RcFZwQWRoYTFNM3M2SDBFQUVHSl9NIn0.eyJqdGkiOiJhMzhiZGM3My0xNjRlLTQwMjItODkzOS1kYjNlZWM2ZGMzMWIiLCJleHAiOjE1MjUzODExMTYsIm5iZiI6MCwiaWF0IjoxNTI1MzgwODE2LCJpc3MiOiJodHRwczovLzU0LjIxNS4yMTMuMjA5Ojg0NDMvYXV0aC9yZWFsbXMvY2lyY3Vsby1jcmVkaXRvLWFwaSIsImF1ZCI6ImNpcmN1bG8tY3JlZGl0by1hcGkiLCJzdWIiOiI4YjQwZTRlNy1hMTk1LTRiYzUtOGUwZC0yZjA5ZDQ3OGI0OGIiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJjaXJjdWxvLWNyZWRpdG8tYXBpIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYjc2NmIzMTYtYjJiOC00NzQ4LTlkMzctOTcyMzM4MDI0ZDg0IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6W10sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJjb25zdWx0YV9leHBlZGllbnRlIiwiYXV0ZW50aWNhY2lvbl9vdG9yZ2FudGUiXX0sInJlc291cmNlX2FjY2VzcyI6e30sImNsaWVudElkIjoiY2lyY3Vsby1jcmVkaXRvLWFwaSIsImNsaWVudEhvc3QiOiIyMDEuMTQ5LjE5LjI1MSIsInByZWZlcnJlZF91c2VybmFtZSI6InNlcnZpY2UtYWNjb3VudC1jaXJjdWxvLWNyZWRpdG8tYXBpIiwiY2xpZW50QWRkcmVzcyI6IjIwMS4xNDkuMTkuMjUxIiwiZW1haWwiOiJzZXJ2aWNlLWFjY291bnQtY2lyY3Vsby1jcmVkaXRvLWFwaUBwbGFjZWhvbGRlci5vcmcifQ.o2AYTcnXFzoHgTDQE-ZtuqzsaIIs0rJ9DAhDRfu2IsY2HPXKP2YQGP2pl2umEZO4_RywUI0i_X01i2so0QiHfFjC12lR3jyI0rhUc20os7ArxtHNSmm82DoJ18KgNTE3sUxOBqo2rVWFj8p16EuE3ycIqvjAM_sGracyraSMU0yyctJGlRjYGILUMmwrOMmN0XSeuZMxXQlmMvSO4zQr20EUvstHLymFkXCKiUoHltPVaWE-xEjy8n7kRbBo1_YUI9zrIcl5tsA1zS8WeK2JLrkTYFH3xut-fJQ2yMyB0Ja2BbVEpQXwvDxxh_DNolBGXNJXqJkxfuiFWhFGnTElGQ' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: UqBaZg3Xaf5SMhQ2TBOU825Rj1J851L59nD6FqH5' \
  -d '{{CUERPO_DEL_SERVICIO_A_USAR}}'
```

4) Al verificar el token de acceso y obtener una respuesta de token inválido o al recibir un código 403 al invocar un servicio podemos saber que el token de acceso se encuentra caduco y debe ser refrescado, es posible usar el token de refresco para este fin 
```curl
curl -X POST \
  https://3ukk53u1hf.execute-api.us-west-1.amazonaws.com/beta-i/auth/realms/circulo-credito-api/protocol/openid-connect/token \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'x-api-key: UqBaZg3Xaf5SMhQ2TBOU825Rj1J851L59nD6FqH5' \
  -d 'grant_type=refresh_token&client_id=circulo-credito-api&client_secret=e3f178a9-93ad-4acc-b7f4-5979165e239e&refresh_token={{eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWckVoaFZrMHJzNGJwX0wzNi1IXy1RcFZwQWRoYTFNM3M2SDBFQUVHSl9NIn0.eyJqdGkiOiIxY2E1Y2M2NC1iYjY1LTQ5NGQtYjE4MC00YTBhN2I2NjMwOTUiLCJleHAiOjE1MjY5OTU3MzcsIm5iZiI6MCwiaWF0IjoxNTI2OTkzOTM3LCJpc3MiOiJodHRwczovL2VpczBjeGpjc2EuZXhlY3V0ZS1hcGkudXMtd2VzdC0xLmFtYXpvbmF3cy5jb20vYXV0aC9yZWFsbXMvY2lyY3Vsby1jcmVkaXRvLWFwaSIsImF1ZCI6ImNpcmN1bG8tY3JlZGl0by1hcGkiLCJzdWIiOiJhY2Q3Y2MxNS0wZGM2LTRhZTItYTUxNy02OGVkY2JmOWNlOWIiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiY2lyY3Vsby1jcmVkaXRvLWFwaSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImUyNWQ4ODJmLTdjNTQtNDQ0YS1iMGNlLWY1YWJlNzUxNjgwNSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJhY3R1YWxpemFjaW9uX2luZm9ybWFjaW9uX2NyZWRpdGljaWEiLCJjb25zdWx0YV9leHBlZGllbnRlIiwiYXV0ZW50aWNhY2lvbl9vdG9yZ2FudGUiXX0sInJlc291cmNlX2FjY2VzcyI6e319.gw48h4Y8mghf8y77XzGeKDZzmwnKkc9HVDY087uoOr4wnFp0jFcFBx6FXsfLty9siPjQodCsTtDGO0-Uqd0UV0i6XJzpE1TYjm3-OQh7OodRO_fKo7X4USjQpZwfTkBR2n9TkgQrWDW8CUpZzQffdcyfx6GQ9wxYf28XpQsYms9gr1rrZ8QLlUoYaS6HuF4hNB23VQqLthcSZgDBlGyp-IwIS15bv_SgxpJRTd7XEHska8vtT2vmXt8mLVVCf3WHG8sKglXEQaMY0otEBALPGUdMWEAU53oHhzeF4rdGDSd0O2WcNa7zWC9ZxO3KIwCJOP-kHQTSV-ViQhyYMVmeMA}}'
```


---

# Configuraciones para el ambiente de pruebas

Las siguientes configuraciones son útiles únicamente con fin demostrativo y no impactaran datos productivos, las configuraciones productivas serán entregadas a cada cliente y el mismo deberá mantenerlas privadas.

|Configuracion|Descripcion|Valor|
|-------------|-----------|-----|
|HOST|Ubicación del servicio de autorización|https://eis0cxjcsa.execute-api.us-west-1.amazonaws.com|
|AMBIENTE|Contexto que permite identificar el ambiente al que pertenecen los datos y servicios(PRUEBAS,BETA,PRODUCCION)|beta-i|
|TIPO_CREDENCIALES|El tipo de credenciales que se usaran (password, client_credentials, implicit, refresh_token), descritos en la especificacion de [OAUT2](https://tools.ietf.org/html/rfc6749#section-1.3)|password|
|USUARIO|Usuario que se enrolara para cada cliente|circulo-user|
|PASSWORD|Contraseña del usuario de cada cliente|circulo-pass|
|CLIENTE|Identificador de cliente, permite consumir la api como un cliente o como agrupador de distintos usuarios que autorizaron el consumo a su nombre|circulo-credito-api|
|LLAVE_CLIENTE|Token del cliente |e3f178a9-93ad-4acc-b7f4-5979165e239e|
|LLAVE_USO|Token que permite asociar el consumo de un cliente a un límite de consumo y también cotizar los cargos de consumo|05KFJtQJBO3F2BSckYpoO1AD8gfFCwL14HuqKacD|