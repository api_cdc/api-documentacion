# Autenticación Otorgante

## Descripción

Asegurar que quien va a recibir el crédito es quien dice ser, mediante un conjunto de preguntas y respuestas proporcionadas por el otorgante.

El servicio cuenta con dos etapas una de generación de preguntas y otro la evaluación de estas.

## Generar Preguntas

La generación de preguntas es el primer paso para la autenticación de la persona.

## URL
> `https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/autenticacion/otorgante`

## HTTP Method: POST

## Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|claveOtorgante|String|Si|**Clave del Otorgante**
|folioOtorgante|String|Si|**folioOtorgante**
|**persona**|Objeto|Si|**Información de la persona**
|└─primerNombre|String|Si|El **primer nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─segundoNombre|String|No|El **segundo nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─apellidoPaterno|String|Si|El **apellido paterno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoMaterno|String|No|El **apellido materno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoAdicional|String|No|Se debe utilizar para reportar el **apellido de casada**.
|└─fechaNacimiento|String|Si|**Fecha de nacimiento** formato: yyyy-MM-dd
|└─rfc|String|No|Reglas:<br>1\.Las primeras 4 posiciones deben ser alfabéticas.<br>2\. 5 y 6 deben contener un número entre 00 y 99.<br>3\. 7 y 8 deben contener un número entre 01 a 12.<br>4\. 9 y 10 deben contener un número entre 01 a 31.  <br>5\. 11 -13 homoclave (opcional). <br>6\. Los RFC's de personas extranjeras deben cumplir con las características arriba mencionadas.|
|└─direccion|String|Si|La **dirección de la persona** incluyendo nombre de la calle, número exterior y/o interior. Deben considerarse avenida, cerrada, manzana, lote, edificio, departamento etc.
|└─colonia|String|No|La **colonia** a la cual pertenece la dirección de la persona
|└─ciudad|String|No|La **ciudad** a la cual pertenece la dirección de la persona
|└─codigoPostal|String|Si|El **código postal** reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado.  <br>En caso de una longitud de 4 dígitos completar con cero a la izquierda (08564)
|└─municipio|String|No|La **delegación** a la pertenece la dirección de la persona
|└─estado|String|Si|La abreviatura correspondiente
|└─pais|String|Si|Debe contener el país donde se encuentra el domicilio del Acreditado
|└─curp|String|No|Reglas: <br>  1- 4 posiciones deben ser alfabéticas.  <br>2- 5 y 6 posiciones deben contener un número entre 00 y 99 (año).<br>3- 7 y 8 posiciones deben contener un número entre 01 y 12 (mes).<br>4- 9 y 10 posiciones deben contener un número entre 01 y 31 (día).  <br>5- 11-16 posiciones deben ser alfabéticas.  <br>6- 17 numérico (homoclave).  <br>7- 18 numérico (Dígito Verificador).


## Ejemplo Request - JSON

```bash
curl -X POST \
  'https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/autenticacion/otorgante' \
  -H 'Authorization: {{TOKEN}}' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: {{API_KEY}}' \
  -d '{
	"claveOtorgante":"{{cveOtorgante}}",
	"folioOtorgante":"{{folio}}",
	"persona":{
		"primerNombre":"{{primerNombre}}",
		"segundoNombre":"{{segundoNombre}}",
		"apellidoPaterno":"{{apellidoPaterno}}",
		"apellidoMaterno":"{{apellidoMaterno}}",
		"apellidoAdicional":"{{apellidoAdicional}}",
		"fechaNacimiento":"{{fechaNacimiento}}",
		"direccion":"{{direccion}}",
		"colonia":"{{colonia}}",
		"ciudad":"{{ciudad}}",
		"codigoPostal":{{codigoPostal}},
		"municipio":"{{municipio}}",
		"estado":"{{estado}}",
		"rfc":"{{rfc}}",
		"pais":"{{pais}}",
		"curp": "{{curp}}"
	}

}'
```
  **Nota:** Se reemplazan las "{{value}}" por el valor correspondiente


## Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|numSolicitud|int|**Número de Solicitud**
|numAutenticacion|String|**Número de autenticación**
|intentos|int|Intentos realizados el maximo de intentos son 3
|opcionesTC|Objeto|Opciones de pregunta **¿Cuántas tarjetas de crédito tienes vigentes?**
|**otorganteTC**|Objeto Otorgante|Otorgante de Tarjeta de Crédito para validar tarjeta de credito
|└─cveOtorgante|String|**Clave del otorgante**
|└─nombOtorgante|String|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|T=Tarjeta de crédito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuatroDigitosTC|String|**Proporciona los último 4 dígitos de tu tarjeta de crédito con “NOMBRE OTORGANTE”**
|creditoHipotecario|boolean|Si tiene Crédito Hipotecario
|**otorganteHB**|Objeto Otorgante|Otorgante de Credito Hipotecario
|└─cveOtorgante|String|**Clave del otorgante**
|└─nombOtorgante|String|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|opcionesCuandoCreoHB|Objeto|Opciones de para la pregunta **¿Hace cuanto abriste tu crédito con “NOMBRE OTORGANTE HIPOTECARIO”?**
|**opcionesConQuienTieneHB**|Arreglo de Otorgante|Opciones para la pregunta **¿Con cuál de ellos tienes crédito hipotecario?**
|└─cveOtorgante|String|**Clave del otorgante**
|└─nombOtorgante|String|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|T=Tarjeta de credito<br>A=Crédito Automotriz<br>H=Crédito Hipotecario
|opcionesCuandoCreoAB|Objeto|Opciones para la pregunta **¿Hace cuanto que abriste el crédito con “NOMBRE OTORGANTE AUTOMOTRIZ”?**
|**otorganteAB**|Objeto Otorgante|otorgante de credito automotriz
|└─cveOtorgante|String|**Clave del otorgante**
|└─nombOtorgante|String|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|opcionesCierreAB|Objeto|Opciones para la pregunta **¿Hace cuanto que terminaste el crédito con “NOMBRE OTORGANTE AUTOMOTRIZ”?**
|**opcionesConQuienTieneAB**|Arreglo de Otorgante|Opciones para la pregunta **¿Con cuál de estas empresas tienes crédito automotriz**
|└─cveOtorgante|String|Clave del otorgante
|└─nombOtorgante|String|Nombre corto del otorgante
|└─tipoNegocioOtor|String|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario

## Ejemplo Response - JSON

```bash
{
    "response": {
        "isSuccess": true,
        "success": {
            "title": "Petición exitosa",
            "message": "Se conecto de forma correcta"
        },
        "errors": [],
        "data": {
            "numSolicitud": "0",
            "opcionesTC": {
                "a": "Entre 1-3",
                "b": "Entre 3-5",
                "c": "No tiene"
            },
            "otorganteTC": {
                "cveOtorgante": "000082",
                "nombOtorgante": "CIUDAD DE PARIS",
                "tipoNegocioOtor": "T"
            },
            "cuatroDigitosTC": "0000",
            "creditoHipotecario": false,
            "otorganteHB": {
                "cveOtorgante": "000391",
                "nombOtorgante": "INVIDF",
                "tipoNegocioOtor": "H"
            },
            "opcionesCuandoCreoHB": {
                "a": "1-3 años",
                "b": "más de 3 años",
                "c": "No tengo ese crédito"
            },
            "opcionesConQuienTieneHB": [
                {
                    "cveOtorgante": null,
                    "nombOtorgante": "Ninguno",
                    "tipoNegocioOtor": "H"
                },
                {
                    "cveOtorgante": "000391",
                    "nombOtorgante": "INVIDF",
                    "tipoNegocioOtor": "H"
                },
                {
                    "cveOtorgante": "000063",
                    "nombOtorgante": "FINPATRIA",
                    "tipoNegocioOtor": "H"
                },
                {
                    "cveOtorgante": "000876",
                    "nombOtorgante": "METROFINANCIERA",
                    "tipoNegocioOtor": "H"
                }
            ],
            "opcionesCuandoCreoAB": {
                "a": "menos de 1 año",
                "b": "entre 1 y 3 años",
                "c": "más de tres años",
                "d": "no tengo"
            },
            "otorganteAB": {
                "cveOtorgante": "000728",
                "nombOtorgante": "LA ESPERANZA",
                "tipoNegocioOtor": "A"
            },
            "opcionesCierreAB": {
                "a": "menos de 1 año",
                "b": "entre 1 y 3 años",
                "c": "más de tres años",
                "d": "no tuve ese crédito"
            },
            "opcionesConQuienTieneAB": [
                {
                    "cveOtorgante": null,
                    "nombOtorgante": "Ninguno",
                    "tipoNegocioOtor": "A"
                },
                {
                    "cveOtorgante": "000728",
                    "nombOtorgante": "LA ESPERANZA",
                    "tipoNegocioOtor": "A"
                },
                {
                    "cveOtorgante": "000009",
                    "nombOtorgante": "BANCO AZTECA",
                    "tipoNegocioOtor": "A"
                },
                {
                    "cveOtorgante": "000080",
                    "nombOtorgante": "MOTOCICLETAS VELASCO",
                    "tipoNegocioOtor": "A"
                }
            ],
            "numAutenticacion": "97735"
        }
    }
}
```


## Evaluar Preguntas

La evaluación de preguntas es el segundo paso para la autenticación de la persona.

## URL
> `https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/autenticacion/otorgante/{{numAutenticacion}}`

## HTTP Method: PUT

## Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|numAutenticacion|String|Si|**Número de Autenticación**
|cuantasTCtiene|String|Si|Almacena el indice de la respuesta "**a,b,c,** o **d**" de la pregunta: **¿Cuántas tarjetas de crédito tienes vigentes? **
|**otorganteTC**|Objeto Otorgante|Si|Otorgante de Tarjeta de Crédito que se valida
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuatroDigitosTC|String|No|Respuestapara la pregunta: **Proporciona los último 4 dígitos de tu tarjeta de crédito con “NOMBRE OTORGANTE A VALIDAR”**
|creditoHipotecario|boolean|Si|Respuesta a la pregunta: **¿estas pagando algún crédito hipotecario?**
|**otorganteHB**|Objeto Otorgante|Si|Otorgante de Crédito Hipotecario a validar
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuandoCreoHB|String|Si|Almacena el indice de la respuesta "**a,b,c,** o **d**" para la pregunta **¿Hace cuanto abriste tu crédito con “NOMBRE OTORGANTE HIPOTECARIO”?**
|**conQuienTieneHB**|Objeto Otorgante|Si|Respuesta a la pregunta: **¿Con cuál de ellos tienes crédito hipotecario?**
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuandoCreoAB|String|Si|Almacena el indice de la respuesta "**a,b,c,** o **d**" para la pregunta **¿Hace cuanto que abriste el crédito con “NOMBRE OTORGANTE AUTOMOTRIZ”?**
|**otorganteAB**|Objeto Otorgante|Si|otorgante de crédito automotriz a validar
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuandoTerminaAB|String|Si|Almacena el índice de la respuesta "**a,b,c,** o **d**" para la pregunta **¿Hace cuánto que terminaste el crédito con “NOMBRE OTORGANTE AUTOMOTRIZ”?**
|**conquienTieneAB**|Objeto Otorgante|Si|Respuesta a la pregunta **¿Con cuál de estas empresas tienes crédito automotriz?**
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario

## Ejemplo Request - JSON

```bash
curl -X PUT \
  'https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/autenticacion/otorgante/{{numAutenticacion}}' \
    -H 'Authorization: {{TOKEN}}' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: {{API_KEY}}' \
  -d '{
  "cuantasTCtiene":"{{cuantasTCtiene}}",
  "otorganteTC":{
    "cveOtorgante":"{{otorganteTC.cveOtorgante}}",
    "nombOtorgante":"{{otorganteTC.nombOtorgante}}",
    "tipoNegocioOtor":"{{otorganteTC.tipoNegocioOtor}}"
  },
  "cuatroDigitosTC":"{{cuatroDigitosTC}}",
  "creditoHipotecario":{{creditoHipotecario}},
  "otorganteHB":{
    "cveOtorgante":"{{otorganteHB.cveOtorgante}}",
    "nombOtorgante":"{{otorganteHB.nombOtorgante}}",
    "tipoNegocioOtor":"{{otorganteHB.tipoNegocioOtor}}"
  },
  "cuandoCreoHB":"{{cuandoCreoHB}}",
  "conquienTieneHB":{
    "cveOtorgante":"{{conquienTieneHB.cveOtorgante}}",
    "nombOtorgante":"{{conquienTieneHB.nombOtorgante}}",
    "tipoNegocioOtor":"{{conquienTieneHB.tipoNegocioOtor}}"
  }, 
  "cuandoCreoAB":"{{cuandoCreoAB}}", 
  "otorganteAB":{
    "cveOtorgante":"{{otorganteAB.cveOtorgante}}",
    "nombOtorgante":"{{otorganteAB.nombOtorgante}}",
    "tipoNegocioOtor":"otorganteAB.tipoNegocioOtor"
  }, 
  "cuandoTerminaAB":"{{cuandoTerminaAB}}",
  "conquienTieneAB":{
    "cveOtorgante":"{{conquienTieneAB.cveOtorgante}}",
    "nombOtorgante":"{{conquienTieneAB.nombOtorgante}}",
    "tipoNegocioOtor":"{{conquienTieneAB.tipoNegocioOtor}}"
  },
  "numAutenticacion":"{{numAutenticacion}}"
}'
```
  **Nota:** Se reemplazan las "{{value}}" por el valor correspondiente


## Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|autenticacion|Boolean|Regresa verdadero si este autentica
|numAutenticacion|String|Número de autenticación
|intentos|int|Numero de intentos de la solicitud anteriores

## Ejemplo Response - JSON

```bash
{
	"response":{
		"isSuccess":true,"success":{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":{
			"intentos":0,
			"numeroAutenticacion":"97156",
			"autenticacion":false
		}
	}
}
```