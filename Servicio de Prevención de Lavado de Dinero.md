﻿﻿# PLD (Prevención de Lavado de Dinero)

## Descripción

Realiza una búsqueda mediante los datos básicos de una persona para saber si se encuentra dentro del servicio PLD.


## Solicitud simple rest

Se realiza la consulta mediante una solicitud simple **Rest**.

## URL
> `https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/pld/interno`

## HTTP Method: POST

## Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|nombres|String|Si|**Nombre(s)** de la persona que se desea buscar. 
|apellidoPaterno|String|Si|**Apellido Paterno** de la persona.
|apellidoMaterno|String|Si|**Apellido Materno** de la persona.


#### Ejemplo Request - JSON

```bash
curl -X POST \
  https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/pld/interno \
  -H 'Authorization: {{token}}' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: {{api-key}}' \
  -d '{
	"nombres":"ANDRES",
	"apellidoPaterno":"LOPEZ",
	"apellidoMaterno":"OBRADOR"
} '
```

  **Nota:** Se reemplazan las "{{value}}" por el valor correspondiente

## Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|**id_Persona**|String|Número de identificación  de la persona
|**peso1**|String|Campo reservado
|**peso2**|String|Campo reservado
|**nombre**|String|Nombre de la persona
|**paterno**|String|Apellido paterno de la persona
|**materno**|String|Apellido materno de la persona
|**CURP**|String|Clave única de registro de población de la persona
|**RFC**|String|Registro federal del contribuyente de la persona
|**sexo**|String|Sexo de la persona
|**lista**|String|Lista en la que se encuentra
|**estatus**|String|Información adicional del registro
|**dependencia**|String|Dependencia en la que labora o laboró
|**puesto**|String|Cargo que desempeña o desempeñó el funcionario sancionado
|**area**|String|Área en la que labora la persona
|**idDispo**|String|Referencia a disposiciones de SHCP. Solo se utiliza en la lista PEP
|**idRel**|String|Numero de identificación con el que esta persona tiene parentesco
|**parentesco**|String|Tipo de parentesco con la persona relacionada
|**RFCMoral**|String|Registro federal del contribuyente en caso de ser persona moral
|**ISSSTE**|String|Numero de seguridad social asignado por el ISSSTE
|**IMSS**|String|Numero de seguridad social asignado por el IMSS
|**ingresos**|String|Ingreso reportado por la dependencia en la que labora
|**nombreComp**|String|Nombre completo de la persona
|**apellidos**|String|Apellidos (materno y paterno) de la persona
|**entidad**|String|Entidad federativa donde se ubica la dependencia
|**CURP_OK**|String|Estatus CURP validada (0-No, 1-Si)
|**periodo**|String|Periodo en el que desempeñó funciones
|**expediente**|String|
|**causa_Irregularidad**|String|Motivo de la sanción al funcionario
|**duracion**|String|Tiempo que se determino como sanción
|**autoridad_Sanc**|String|Organo que dicta la sanción


## Ejemplo Response - JSON

```bash
{
    "response": {
        "isSuccess": true,
        "success": {
            "title": "Petición exitosa",
            "message": "Se conecto de forma correcta"
        },
        "errors": [],
        "data": {
            "ocurrioError": false,
            "personas": [
                {
                    "id_Persona": "QEQ0237305",
                    "peso1": 0,
                    "peso2": 0,
                    "nombre": "ANDRES MANUEL",
                    "paterno": "LÓPEZ",
                    "materno": "OBRADOR",
                    "sexo": "MASCULINO",
                    "lista": "PEP",
                    "estatus": "ACTIVO",
                    "dependencia": " ",
                    "puesto": "OTRO",
                    "area": " ",
                    "idDispo": "6",
                    "idRel": "QEQ0242614",
                    "parentesco": "CUÑADO",
                    "ingresos": " ",
                    "nombreComp": "ANDRES MANUEL LÓPEZ OBRADOR",
                    "apellidos": "LÓPEZ OBRADOR",
                    "entidad": " ",
                    "periodo": " ",
                    "expediente": " ",
                    "causa_Irregularidad": " ",
                    "duracion": " ",
                    "autoridad_Sanc": " ",
                    "curp_OK": 0
                },
                {
                    "id_Persona": "QEQ0180416",
                    "peso1": 0,
                    "peso2": 0,
                    "nombre": "ANDRÉS MANUEL",
                    "paterno": "LÓPEZ",
                    "materno": "OBRADOR",
                    "sexo": "MASCULINO",
                    "lista": "PEP",
                    "estatus": "ACTIVO",
                    "dependencia": "PARTIDO MOVIMIENTO REGENERACIÓN NACIONAL",
                    "puesto": "PRESIDENTE DEL CONSEJO NACIONAL",
                    "area": "OFICINA DEL C. PRESIDENTE DEL CONSEJO NACIONAL",
                    "idDispo": "551",
                    "idRel": " ",
                    "parentesco": " ",
                    "ingresos": " ",
                    "nombreComp": "ANDRÉS MANUEL LÓPEZ OBRADOR",
                    "apellidos": "LÓPEZ OBRADOR",
                    "entidad": "DISTRITO FEDERAL",
                    "periodo": " ",
                    "expediente": " ",
                    "causa_Irregularidad": " ",
                    "duracion": " ",
                    "autoridad_Sanc": " ",
                    "curp_OK": 0
                },
                {
                    "id_Persona": "QEQ0135989",
                    "peso1": 0,
                    "peso2": 0,
                    "nombre": "ANDRÉS MANUEL",
                    "paterno": "LÓPEZ",
                    "materno": "OBRADOR",
                    "sexo": "MASCULINO",
                    "lista": "VENC",
                    "estatus": "11 AÑOS",
                    "dependencia": "Candidatos",
                    "puesto": "CANDIDATOS A PRESIDENTE DE LA REPÚBLICA 2006",
                    "area": "PARTIDOS POLÍTICOS/CANDIDATOS 2006-2009",
                    "idDispo": "561",
                    "idRel": " ",
                    "parentesco": " ",
                    "ingresos": " ",
                    "nombreComp": "ANDRÉS MANUEL LÓPEZ OBRADOR",
                    "apellidos": "LÓPEZ OBRADOR",
                    "entidad": " ",
                    "periodo": " ",
                    "expediente": " ",
                    "causa_Irregularidad": " ",
                    "duracion": " ",
                    "autoridad_Sanc": " ",
                    "curp_OK": 0
                },
                {
                    "id_Persona": "QEQ0120591",
                    "peso1": 0,
                    "peso2": 0,
                    "nombre": "ANDRÉS MANUEL",
                    "paterno": "LÓPEZ",
                    "materno": "OBRADOR",
                    "sexo": "MASCULINO",
                    "lista": "VENC",
                    "estatus": "5 AÑOS",
                    "dependencia": "Candidatos",
                    "puesto": "CANDIDATOS A PRESIDENTE DE LA REPÚBLICA 2012 MOVIMIENTO PROGRESISTA",
                    "area": "PARTIDOS POLÍTICOS/CANDIDATOS A PRESIDENTE DE LA REPÚBLICA",
                    "idDispo": "511",
                    "idRel": " ",
                    "parentesco": " ",
                    "ingresos": " ",
                    "nombreComp": "ANDRÉS MANUEL LÓPEZ OBRADOR",
                    "apellidos": "LÓPEZ OBRADOR",
                    "entidad": " ",
                    "periodo": " ",
                    "expediente": " ",
                    "causa_Irregularidad": " ",
                    "duracion": " ",
                    "autoridad_Sanc": " ",
                    "curp_OK": 0
                }
            ]
        }
    }
}
```