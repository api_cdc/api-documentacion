﻿﻿# Consumo Consultas

## Descripción

Realiza una búsqueda en la bitácora del otorgante para mostrarle las consultas que se han realizado indicando mínimo el otorgante y el periodo, de manera opcional podrá indicar el tipo de producto o en su defecto el nombre del mismo.


## Solicitud simple rest

Se realiza la consulta mediante una solicitud simple **Rest**.

## URL
> `https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/consumoConsultas`

## HTTP Method: POST

## Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|periodo|String|Si|**Periodo** de búsqueda hacia atrás, se indica el número seguido de un espacio y la letra del tipo de unidad en máyuscula. <br> ***D*** -> Se ocupa para especificar días atrás, el máximo de días es 31. <br> ***M*** -> Se ocupa para especificar meses atrás, el máximo de meses es 12. <br> ***Y*** -> Se ocupa para especificar años atrás, el máximo de años es 1. <br> ***W*** -> Se ocupa para especificar semanas atrás, el máximo de semanas es 52.
|fechaIni|String|Si|**Fecha inicial** donde empezará la consulta. Su formato de fecha debe ser **"yyyy-MM-dd"**.
|fechaFin|String|Si|**Fecha final** donde terminará la consulta. Su formato de fecha debe ser **"yyyy-MM-dd"**. De no ser especificado este campo se tomará la fecha actual en que se realiza la consulta y para asignarla como fecha final.
|tipo|String|No|**Tipo** de producto a consultar.
|producto|String|No|Nombre del **Producto** a consultar.
|otorgante|String|Si|**Otorgante** encargado de otorgar el crédito.

#### Ejemplo Request - JSON

```bash
curl -X POST \
  https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/consumoConsultas \
  -H 'Authorization: {{token}}' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: {{api-key}}' \
  -d '{
	"periodo":"21 D",
	"fechaIni":"",
	"fechaFin":"",
	"tipo":"",
	"producto":"",
	"otorgante":"001"
} '
```

  **Nota:** Se reemplazan las "{{value}}" por el valor correspondiente

## Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|**cuantos**|Long|Número de veces que se realizó el consumo del microservicio
|**tipo**|String|Clave del tipo de producto
|**fecha**|String|La fecha en la que se realizo el consumo


## Ejemplo Response - JSON

```bash
{
    "response": {
        "isSuccess": true,
        "success": {
            "title": "Petición exitosa",
            "message": "Se conecto de forma correcta"
        },
        "errors": [],
        "data": {
            "success": true,
            "consumos": [
                {
                    "cuantos": 4,
                    "tipo": "PLD",
                    "fecha": "2018-05-25"
                },
                {
                    "cuantos": 16,
                    "tipo": "CO",
                    "fecha": "2018-05-21"
                }
            ]
        }
    }
}
```