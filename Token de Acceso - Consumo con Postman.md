#Consumir los servicios para Obtener un token de acceso 

## Postman

[Postman](https://www.getpostman.com/postman) es un popular cliente Rest, gratuito, que facilita el consumo de servicios Rest.

---
## Importar configuracion

Para facilitar las pruebas de la obtencion de tokens se ha creado la siguiente **configuracion** de Postman
```JSON
{"info": {"_postman_id": "c0c05b38-aef2-4252-b5e1-a1e885c9b30f","name": "Autenticacion","schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"},"item": [{"name": "ObtenerToken-Configuracin","request": {"method": "GET","header": [{"key": "Content-Type","value": "application/x-www-form-urlencoded"},{"key": "x-api-key","value": "{{api-key}}"}],"body": {"mode": "urlencoded","urlencoded": [{"key": "grant_type","value": "client_credentials","type": "text"},{"key": "client_id","value": "circulo-credito-api","type": "text"},{"key": "client_secret","value": "8d627138-7d26-4016-95e1-2ecbf4a29c75","type": "text"}]},"url": {"raw": "{{url}}/auth/realms/{{realm}}/.well-known/openid-configuration","host": ["{{url}}"],"path": ["auth","realms","{{realm}}",".well-known","openid-configuration"]}},"response": []},{"name": "ObtenerToken-Password","request": {"auth": {"type": "noauth"},"method": "POST","header": [{"key": "Content-Type","value": "application/x-www-form-urlencoded"},{"key": "Authorization","value": "c1d6f1e9-4311-4751-a339-ae8b1225cfd8","disabled": true},{"key": "x-api-key","value": "{{api-key}}"}],"body": {"mode": "urlencoded","urlencoded": [{"key": "grant_type","value": "password","type": "text"},{"key": "username","value": "{{user}}","type": "text"},{"key": "password","value": "{{pass}}","type": "text"},{"key": "client_id","value": "{{client}}","type": "text"},{"key": "client_secret","value": "{{secret}}","type": "text"}]},"url": {"raw": "{{url}}/auth/realms/{{realm}}/protocol/openid-connect/token","host": ["{{url}}"],"path": ["auth","realms","{{realm}}","protocol","openid-connect","token"]}},"response": []},{"name": "ObtenerToken-InformacionToken","request": {"auth": {"type": "noauth"},"method": "POST","header": [{"key": "Authorization","value": "Bearer {{token}}"},{"key": "x-api-key","value": "{{api-key}}"}],"body": {"mode": "raw","raw": ""},"url": {"raw": "{{url}}/auth/realms/{{realm}}/protocol/openid-connect/userinfo","host": ["{{url}}"],"path": ["auth","realms","{{realm}}","protocol","openid-connect","userinfo"]}},"response": []}],"event": [{"listen": "prerequest","script": {"id": "4bf639f1-0f2a-4033-9cae-d11258a2c341","type": "text/javascript","exec": [""]}},{"listen": "test","script": {"id": "ff36c5fe-99c2-4b01-89e2-a8b59a3941a2","type": "text/javascript","exec": [""]}}]}
```
Y la configuracion de un **ambiente** de Pruebas se muestra a continuacion
```JSON
{"id": "4d140337-a09a-4f3e-9d48-df7ee13ecf42","name": "Autenticacion - CDC_TEMP","values": [{"key": "url","value": " https://eis0cxjcsa.execute-api.us-west-1.amazonaws.com/beta-i","description": "","type": "text","enabled": true},{"key": "realm","value": "circulo-credito-api","description": "","type": "text","enabled": true},{"key": "realm-key","value": "circulo-credito-api","description": "","type": "text","enabled": true},{"key": "user","value": "circulo-user","description": "","type": "text","enabled": true},{"key": "pass","value": "circulo-pass","description": "","type": "text","enabled": true},{"key": "client","value": "circulo-credito-api","description": "","type": "text","enabled": true},{"key": "secret","value": "e3f178a9-93ad-4acc-b7f4-5979165e239e","description": "","type": "text","enabled": true},{"key": "token","value": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWckVoaFZrMHJzNGJwX0wzNi1IXy1RcFZwQWRoYTFNM3M2SDBFQUVHSl9NIn0.eyJqdGkiOiJjNjUxNzI5Yi01ODM2LTRmZmYtYjEyYS0wMTQ4NGZmOGU3ZmIiLCJleHAiOjE1MjcxNjUxMTAsIm5iZiI6MCwiaWF0IjoxNTI3MTY0ODEwLCJpc3MiOiJodHRwczovL2VpczBjeGpjc2EuZXhlY3V0ZS1hcGkudXMtd2VzdC0xLmFtYXpvbmF3cy5jb20vYXV0aC9yZWFsbXMvY2lyY3Vsby1jcmVkaXRvLWFwaSIsImF1ZCI6ImNpcmN1bG8tY3JlZGl0by1hcGkiLCJzdWIiOiJhY2Q3Y2MxNS0wZGM2LTRhZTItYTUxNy02OGVkY2JmOWNlOWIiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJjaXJjdWxvLWNyZWRpdG8tYXBpIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiMzkxNGMwNDQtOTIzYy00OTc4LTk5OGItOTlhNmI2ODM5YTE2IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6W10sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJhY3R1YWxpemFjaW9uX2luZm9ybWFjaW9uX2NyZWRpdGljaWEiLCJjb25zdWx0YV9leHBlZGllbnRlIiwiYXV0ZW50aWNhY2lvbl9vdG9yZ2FudGUiXX0sInJlc291cmNlX2FjY2VzcyI6e30sIm5hbWUiOiJjaXJjdWxvdXNlciBjaXJjdWxvdXNlciIsInByZWZlcnJlZF91c2VybmFtZSI6ImNpcmN1bG8tdXNlciIsImdpdmVuX25hbWUiOiJjaXJjdWxvdXNlciIsImZhbWlseV9uYW1lIjoiY2lyY3Vsb3VzZXIiLCJlbWFpbCI6ImNpcmN1bG91c2VyQGNpcmN1bG91c2VyIn0.BgKbKDIEeuRNsQMYEOyH_7Tl_AS2ATuffBC2ubiFYzE-HkAu8RRTsOs1fUv12yuQKhfTTgKr9j5vjJOVu-mdCZMqs1_eZHNwefhszoV74XHvDHd_Fgx5_89s7HkxlWZtuK0OBn-9v164fwASM47MdHuSkEPJ_PuOVVaNvFe4q4xk-3H452ncPzMM7GHuz0AgU2eKKdQMGj9JsNR5Q4ZRHcCkW3IX7VIOZHxlRzjCZd-y_RyKpUfh533hdT8d25LYyDh_ZsklPz2dLdGesx53dPVAVEaG-0awXa-22URWBZqoOeYmJPCaN0xW1GSxqFSsntwNRa6RTExcX7DWljB9lA","description": "","type": "text","enabled": true},{"key": "api-key","value": "05KFJtQJBO3F2BSckYpoO1AD8gfFCwL14HuqKacD","description": "","type": "text","enabled": true}],"_postman_variable_scope": "environment","_postman_exported_at": "2018-05-24T12:35:39.302Z","_postman_exported_using": "Postman/6.1.2"}
```

---
## Proceso para importar las configuraciones

1) El primer paso es hacer click en el boton importar como se muestra en la siguiente imagen.

![Postman1.png](https://bitbucket.org/repo/9pnkn8B/images/318628381-Postman1.png)



2) Se debe copiar el texto de la **configuracion** y posteriormente la de el **ambiente** en el campo de texto mostrado en la imagen dentro de la seccion **Pegar Texto Plano (Paste Raw Text)**

![Postman1-2.png](https://bitbucket.org/repo/9pnkn8B/images/3718611710-Postman1-2.png)



3) Una ves importados ambos archivos se tendran disponibles, una coleccion (conjunto de peticiones)
y un ambiente(conjunto de variables)

![Postman4.png](https://bitbucket.org/repo/9pnkn8B/images/3359855318-Postman4.png)



4) Al seleccionar un elemento de la coleccion este mostra informacion util de la peticion, 
como lo son los Encabezados y el Cuerpo de la peticion, usando el boton Enviar podemos ejecutar una peticion.

![Postman3.png](https://bitbucket.org/repo/9pnkn8B/images/4151100506-Postman3.png)


Adicionalmente Postman cuenta con una herramienta muy util que permite exportar la consulta REST a 
distintos lenguajes, entre ellos cURL, una herramienta de consola que tambien permite ejecutar peticiones

![Postman5.png](https://bitbucket.org/repo/9pnkn8B/images/485965948-Postman5.png)