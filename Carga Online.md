﻿# Carga Online

## Descripción
El microservicio de Carga Online se encarga de la actualización en linea, sobre la información de un otorgante sobre las operaciones crediticias. Para realizar dicha actualización se deben depurar la calidad de información antes de efectuar el guardado en la base de datos de Círculo de Crédito.

## URL
>`https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/carga-online`

## HTTP Method: POST

### Request Parameters
|Encabezado||||
|-------|:-------:|:-------:|-------|
|Propiedad|Tipo|¿Requerido?|Descripción|
|**Encabezado**||||
|└─cveOtorgante|String|Si|La **clave del otorgante** con la que se afilio a circulo de crédito.
|└─nombreOtorgante|String|Si|Es el nombre corto con el que Circulo de Crédito denomina y entrega al Usuario que reporta la información.
|└─version|Integer|Si|La **versión** Es el número de la versión donde el valor numérico es 4.
|└─formatoFecha|String|Si|El **formato** de fecha que deberán tener todas las fechas proporcionadas por el otorgante ("dd/MM/yyyy", "MM/dd/yyyy", "yyyy/MM/dd").
|**Datos Generales**||||
|└─apellidoPaterno|String|Si|El **apellido paterno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoMaterno|String|Si|El **apellido materno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoAdicional|String|No|El **apellido de casada** que pudiera tener una persona física.
|└─nombres|String|Si|El **nombre completo del Usuario** (primer nombre, segundo nombre y/o nombres compuestos). No usar abreviaturas, iniciales, acentos y/o puntos.
|└─fechaNacimiento|String|Si|La **fecha de nacimiento** del cliente reportado. De acuerdo al formato de fecha elegido.
|└─rfc|String|Si|El **RFC** del cliente el cual debe constar de 4 posiciones alfabéticas las siguientes 2 deben contener el numero correspondiente aL año de nacimiento del cliente los 4 siguientes corresponden 2 al mes y los otros 2 al día de la fecha de nacimiento. Los últimos 3 opcionales corresponden a la homoclave del cliente.
|└─curp|String|No|La clave **CURP** del cliente.
|└─nss|String|No|El **número de seguridad social** del cliente.
|└─cveNacionalidad|String|Si|La clave de la Nacionalidad del Cliente.
|└─cveResidencia|Integer|No|La situación de la vivienda del Cliente 1 = Propietario, 2 = Renta, 3 = Vive con familiares, 4 = Vivienda hipotecada, 5 = No disponible.
|└─numeroLicenciaConducir|String|No|El número de la licencia de conducir del cliente.
|└─cveEstadoCivil|String|No|El **estado civil** del Cliente. D=Divorciado, L=Unión Libre, C=Casado, S=Soltero, V=Viudo, E=Separado.
|└─sexo|String|No|El **sexo** del cliente. F=Femenino, M=Masculino.
|└─cveElectorIFE|String|No|El numero de la identificación oficial del cliente.
|└─numeroDependientes|Integer|No|El número de personas que dependen economicamente del cliente.
|└─fechaDefuncion|String||No|La fecha de muerte del cliente (en caso de haber fallecido ya).
|└─tipoPersona|String|No|La palabra **PF** para referirse a personas físicas.
|└─indicardorDefuncion|String|No|La frase "si" o "no" para referir si el cliente ya ha fallecido.
|**Domicilios (1 a n)**||||
|└─direccionPersona|String|Si|La **dirección** del Usuario incluyendo nombre de la calle, número exterior y/o interior.
|└─coloniaPersona|String|Si|La **colonia** a la cual pertenece la dirección del Usuario.
|└─municipioPersona|String|Si|El **municipio** a la cual pertenece la dirección del Usuario.
|└─ciudadPersona|String|Si|La **ciudad** a la cual pertenece la dirección del Usuario.
|└─cveEstadoPersona|String|Si|La abreviatura correspondiente al estado de la república al que pertenece la dirección del cliente.
|└─cpPersona|Integer|Si|El **código postal** del domicilio del cliente debe estar compuesto por 5 dígitos, En caso de una longitud de 4 dígitos completar con cero a la izquierda (08564).
|└─fechaResidenciaPersona|String|No|La fecha desde que el cliente vive en dicho domicilio.
|└─telefonoPersona|Long|No|El **número telefónico** del cliente.
|└─cveDomicilioPersona|String|No|La clave del tipo domicilio del Cliente N=Negocio, O=Domicilio del Otorgante, C=Casa, P=Apartado Postal, E = Empleo
|└─cveAsentamientoPersona|Integer|No|valor numérico que comprende del 0 al 55 para especificar el tipo de asentamiento donde reside el cliente. 
|└─cveOrigenDomicilio|String|Si|La clave del país donde se encuentra el domicilio del cliente.
|**Empleos (1 a n)**||||
|└─nombreEmpresa|String|Si|El **nombre o razón social** de la empresa donde labora el cliente, en caso de no tener trabajo debe reportarse Trabajador Independiente, Estudiante, Labores de Hogar, Jubilado, Desempleado o Exempleado.
|└─direccionEmpleo|String|Si|La **dirección de la empresa** donde labora el cliente
|└─coloniaEmpleo|String|Si|La **colonia de la empresa** donde labora el cliente
|└─municipioEmpleo|String|Si|El **municipio de la empresa** donde labora el cliente
|└─ciudadEmpleo|String|Si|La **ciudad de la empresa** donde labora el cliente
|└─cveEstadoEmpleo|String|Si|La clave correspondiente al estado de la república de la empresa donde labora el cliente.
|└─cpEmpleo|Integer|Si|El **código postal** correspondiente al estado de la empresa para la cuál trabaja el empleado.
|└─telefonoEmpleo|Long|No|El **teléfono** del empleo del cliente
|└─extensionEmpleo|Integer|No|La **extensión** del empleo del cliente.
|└─faxEmpleo|Long|No|El **número de fax** del empleo del empleado.
|└─puestoPersona|String|No|El **puesto** que desempeña el cliente.
|└─fechaContratacion|String|No|La fecha en la que fue contratado el cliente por su empleador.
|└─cveMoneda|String|No|Es el **tipo de moneda** que se le paga al Cliente en su empleo. peden ser MX=Pesos Mexicanos, US=Dólares, UD=Unidades de Inversión.
|└─salarioMensual|String|No|El **salario mensual** del cliente.
|└─fechaUltimoEmpleo|String|No|Fecha en la que el cliente laboró por última vez en la empresa.
|└─fechaVerificacionEmpleo|String|No|La fecha en la que se hizo la última verificación del empleo.
|└─cveOrigenEmpleo|String|No|La clave del país dónde se encuentra el domicilio del empleo del cliente.
|**Cuenta (1 a n)**||||
|└─cveActualOtorgante|String|Si|La clave asignada por al círculo al otorgante.
|└─nombreOtorgante|String|Si|El nombre del otorgante.
|└─cuentaActual|String|Si|El número de cuenta.
|└─tipoResponsabilidad|String|Si|La clave del tipo de responsabilidad I=Individual, M=Mancomunado, O=Obligatorio Solidario, A=Aval, T=Titular con Aval
|└─tipoCuenta|String|Si|La clave del tipo de cuenta F=Pagos Fijos, H=Hipoteca, L=Sin Límite Preestablecido, R=Revolvente, Q=Quirografario, A=Crédito de Habilitación o Avío, E=Crédito Refaccionario, P=Crédito Prendario.
|└─tipoContrato|String|Si|La clave que distingue la tipo de contrato que puede ser: AA=Arrendamiento Automotriz., HB=Hipotecario Bancario, AB=Automotriz Bancario, HE=Préstamo Tipo Home Equity, AE=Física Actividad Empresarial, HV=Hipotecario ó Vivienda, AM=Aparatos/Muebles, LC=Línea de Crédito, AR=Arrendamiento, MC=Mejoras a la Casa, AV=Aviación, NG=Préstamo No Garantizado, BC=Banca Comunal, PB=Préstamo Personal Bancario, BL=Bote/Lancha, PC=Procampo, BR=Bienes Raíces, PE=Préstamo Para Estudiante, CA=Compra De Automóvil, PG=Préstamo Garantizado, CC=Crédito Al Consumo, PQ=Préstamo Quirografario, CF=Crédito Fiscal, PM=Préstamo Empresarial, CO=Consolidación, PN=Préstamo de Nómina, PP=Préstamo Personal, ED=Editorial, SH=Segunda Hipoteca, EQ=Equipo, TC=Tarjeta De Crédito, FF=Fondeo Fira, TD=Tarjeta Departamental, FI=Fianza, TG=Tarjeta Garantizada, FT=Factoraje, TS=Tarjeta De Servicios, GS=Grupo Solidario, VR=Vehículo Recreativo, OT=Otros, NC=Desconocido
|└─cveUnidadMonetaria|String|Si|La clave de la unidad monetaria para la cuenta MX = Pesos, US = Dólares, UD = Unidades de inversión
|└─valorActivoValuacion|Integer|no|El valor total del activo para propósitos de evaluación o recuperación.   
|└─numeroPagos|String|no|El numero de pagos efectuados por el cliente.
|└─frecuenciaPagos|String|Si|La periodicidad con la que el cliente debe realizar sus pagos.
|└─montoPagar|Integer|Si|El monto que el cliente debe pagar.
|└─fechaAperturaCuenta|String|Si|La fecha en la que se aperturó la cuenta del cliente
|└─fechaUltimoPago|String|Si|La fecha mas reciente en la que el cliente realizo un pago, se debe reportar la fecha 19010101 en el formato de fecha que halla seleccionado el usuario para sus fechas en este esquema.
|└─fechaUltimaCompra|String|Si|La fecha en la que el cliente efectuó su última compra.
|└─fechaCierreCuenta|String|Si|La fecha en la que se liquido o cerro un crédito.
|└─fechaCorte|String|Si|La fecha del periodo que se esta reportando.
|└─garantia|String|No|El tipo de garantía para el crédito Otorgado.
|└─creditoMaximo|Integer|Si|La cantidad máxima que dispone de crédito el cliente.
|└─saldoActual|Long|Si|Es el importe total del adeudo que tiene el Clientes a la fecha de reporte incluyendo intereses.
|└─limiteCredito|Integer|Si|El límite de crédito que el Usuario extiende al Cliente.
|└─saldoVencido|Integer|Si|La cantidad generada a la fecha de reporte por atraso en pagos.
|└─numeroPagosVencidos|Integer|No|Es el número de pagos que NO ha efectuado el Cliente.
|└─pagoActual|String|Si|El valor del “Pago Actual” es vigente, se debe reportar “V” (espacio”V”), para todos los demás casos, reportar de 01 a 84 pagos.
|└─historicoPagos|String|No|Reportar únicamente en el primer envió de la cuenta a Círculo de Crédito, quien posteriormente lo construirá basándose en la información reportada.
|└─cvePrevencion|String|No|La clave que describe la situación del crédito AD=Cuenta o monto en aclaración, CA=Cartera al Corriente Vendida o cedida a un usuario de una Sociedad, CC=Cuenta cancelada o cerrada, CD=Convenio y disminución de pago, CL=Cuenta cerrada que estuvo en cobranza y fue pagada totalmente sin causar quebranto, CO=Crédito en Controversia, CV=Cuenta que no está al corriente vendida o cedida a un usuario de una Sociedad, FD=Cuenta Fraudulenta, FN=Fraude no Atribuible al Consumidor, FP=Fianza pagada, FR=Adjudicación o aplicación de garantía, GP=Ejecución de Garantía Prendaria o Fiduciaria en Pago por Crédito, IA=Cuenta Inactiva, IM=Integrante causante de mora, IS=Integrante que fue subsidiado para evitar mora, LC=Convenio de finiquito o pago menor acordado con el Consumidor, LG=Pago menor por programa institucional o de gobierno, incluyendo los apoyos a damnificados por catástrofes naturales, LO=En Localización, LS=Tarjeta de Crédito Extraviada o Robada, NA=Cuenta al corriente vendida o cedida a un NO Usuario de una Sociedad, NV=Cuenta que no está al corriente vendida o cedida aunNO Usuario de una Sociedad, PC=Cuenta en Cobranza, PD=Prórroga otorgada debido a un desastre natural, PE=Prórroga otorgada al acreditado por situaciones especiales, PI=Prórroga otorgada al acreditado por invalidez, defunción, PR=Prórroga otorgada debido a una pérdida de relación laboral, RA=Cuenta reestructurada sin pago menor, por programa institucional o gubernamental, incluyendo los apoyos a damnificados por catástrofes naturales, RI=Robo de identidad, RF=Resolución judicial favorable al cliente, RN=Cuenta reestructurada debido a un proceso judicial, RV=Cuenta reestructurada sin pago menor por modificación de la situación del cliente, a petición de éste, SG=Demanda por el Otorgante, UP=Cuenta que causa quebranto, VR=Dación en Pagos ó Renta.
|└─totalPagosReportados|Integer|No|Es el total de pagos realizados por el Cliente a la fecha que se está reportando.
|└─cveAnteriorOtorgante|Long|No|Este elemento se hace requerido cuando existe un cambio en el Usuario.
|└─nombreAnteriorOtorgante|String|No|El campo aplica cuando por algún motivo se modifica el número de Usuario asignado por CC.
|└─numeroCuentaAnterior|String|No|El campo aplica cuando se efectúa una Reasignación de cuenta. Se captura una sola vez y en la siguiente entrega ya no se debe reportar el dato.
|└─fechaPrimerIncumplimiento|String|Si|La fecha en que el consumidor incumplió por primera vez con algún pago. Una vez que ya se reportó una primera fecha ésta no debe modificarse. En caso de no contar con incumplimiento en el crédito deberá de reportar la fecha dummie 19010101 ó 19000101 en el formato seleccionado en el encabezado por el Usuario.
|└─saldoInsoluto|Long|Si|Monto que se adeuda a la fecha de corte, no deberá incluir intereses, comisiones o cualquier otro accesorio. El Saldo Insoluto no puede ser mayor al Saldo Actual, pero sí podrán ser iguales.
|└─montoUltimoPago|Long|Si|Es el pago parcial más reciente que el consumidor efectuó.	
|└─fechaIngresoCarteraVencida|String|No|La fecha en la que el acreditado fue promovido a cartera vencida, en el formato que se seleccionó en el encabezado.
|└─montoCorrespondienteIntereses|Integer|No|Importe correspondiente al monto de los intereses del crédito.
|└─formaPagoActualIntereses|String|No|El código que indica el comportamiento de pago de los intereses. El reporte de este pagó deberá mensualizarse (periodos de 30 días).
|└─diasVencimiento|Integer|No|El número de días que ha estado vencido el crédito, sí este se encuentra al corriente reportar cero 0.	
|└─plazoMeses|Integer|Si|Es el plazo original del crédito se debe de reportar en días.
|└─montoCreditoOriginacion|Long|No|Es el Monto que se otorgó al Cliente en la apertura del crédito.
|└─correoElectronico|String|No|Ingresar la cuenta de correo electrónico del Cliente.





#### Ejemplo Request - JSON

```bash
curl -X POST \
  https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/carga-online \
  -H 'Authorization: {{TOKEN}}' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: {{API_KEY}}' \
  -d '{  
  "encabezado": {
        "claveOtorgante": "001469",
        "nombreOtorgante": "CIRCULO",
        "version": 4,
        "formatoFecha": "yyyy/MM/dd"
    },
   "datosGenerales":{  
      "apellidoPaterno":"JOSE",
      "apellidoMaterno":"SANCHEZ",
      "apellidoAdicional":"Solari",
      "nombres":"JOSE CRUZ",
      "fechaNacimiento":19410722,
      "rfc":"GOSC901102",
      "curp":"HEAC780701MDFRSN01",
      "nss":20064184494,
      "cveNacionalidad":"MX",
      "cveResidencia":1,
      "numeroLicenciaConducir":"G410722098",
      "cveEstadoCivil":"S",
      "sexo":"M",
      "cveElectorIFE":"6152461136363",
      "numeroDependientes":1,
      "fechaDefuncion":20180315,
      "tipoPersona":"PF",
      "indicardorDefuncion":""
   },
   "domicilios":[  
      {  
         "direccionPersona":"JOSE MARIA GAMA 158",
         "coloniaPersona":"Renovación",
         "municipioPersona":"ZACOALCO",
         "ciudadPersona":"ZACOALCO DE TORRES",
         "cveEstadoPersona":"JAL",
         "cpPersona":"45750",
         "fechaResidenciaPersona":"20100725",
         "telefonoPersona":"53146669",
         "cveDomicilioPersona":"C",
         "cveAsentamientoPersona":"6",
         "cveOrigenDomicilio":"MX"
      },
      {  
         "direccionPersona":"JOSE MARIA GAMA 159",
         "municipioPersona":"ZACOALCO",
         "ciudadPersona":"ZACOALCO DE TORRES",
         "cveEstadoPersona":"JAL",
         "cpPersona":"45750",
         "fechaResidenciaPersona":"20100725",
         "telefonoPersona":"53146669",
         "cveDomicilioPersona":"C",
         "cveAsentamientoPersona":"6",
         "cveOrigenDomicilio":"MX"
      },
      {  
         "direccionPersona":"JOSE MARIA GAMA 168",
         "municipioPersona":"ZACOALCO",
         "ciudadPersona":"ZACOALCO DE TORRES",
         "cveEstadoPersona":"JAL",
         "cpPersona":"45750",
         "fechaResidenciaPersona":"20100725",
         "telefonoPersona":"53146669",
         "cveDomicilioPersona":"C",
         "cveAsentamientoPersona":"6",
         "cveOrigenDomicilio":"MX"
      }
   ],
   "empleos":[  
      {  
         "nombreEmpresa":"CIRCULO DE CREDITO",
         "direccionEmpleo":"EJERCITO NACIONAL 904",
         "coloniaEmpleo":"PALMAS POLANCO",
         "municipioEmpleo":"PALMAS POLANCO",
         "ciudadEmpleo":"MEXICO",
         "cveEstadoEmpleo":"CDMX",
         "cpEmpleo":"11560",
         "telefonoEmpleo":"17209900",
         "extensionEmpleo":"9980",
         "faxEmpleo":"",
         "puestoPersona":"ANALISTA DE CALIDAD",
         "fechaContratacion":"20100725",
         "cveMoneda":"MX",
         "salarioMensual":"10000",
         "fechaUltimoEmpleo":"20110725",
         "fechaVerificacionEmpleo":"20120725",
         "cveOrigenEmpleo":"MX"
      }
   ],
   "cuenta":[  
      {  
         "cveActualOtorgante":7730040,
         "nombreOtorgante":"CAJATZAULAN",
         "cuentaActual":"12608003119",
         "tipoResponsabilidad":null,
         "tipoCuenta":"F",
         "tipoContrato":"PP",
         "cveUnidadMonetaria":null,
         "valorActivoValuacion":null,
         "numeroPagos":24,
         "frecuenciaPagos":"M",
         "montoPagar":501,
         "fechaAperturaCuenta":20110503,
         "fechaUltimoPago":20120322,
         "fechaUltimaCompra":20110503,
         "fechaCierreCuenta":null,
         "fechaCorte":20120831,
         "garantia":null,
         "creditoMaximo":10000,
         "saldoActual":3370,
         "limiteCredito":0,
         "saldoVencido":0,
         "numeroPagosVencidos":null,
         "pagoActual":"V",
         "historicoPagos":null,
         "cvePrevencion":null,
         "totalPagosReportados":null,
         "cveAnteriorOtorgante":null,
         "nombreAnteriorOtorgante":null,
         "numeroCuentaAnterior":null,
         "fechaPrimerIncumplimiento":20120731,
         "saldoInsoluto":10000,
         "montoUltimoPago":501,
         "fechaIngresoCarteraVencida":20150101,
         "montoCorrespondienteIntereses":1000,
         "formaPagoActualIntereses":null,
         "diasVencimiento":0,
         "plazoMeses":24,
         "montoCreditoOriginacion":3000,
         "correoElectronico":"luigi204@hotmail.com"
      }
   ]
}'
```
  **Nota:** Se reemplazan las "{{value}}" por el valor correspondiente


#### Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
| **Response**|||
|isSuccess|String|bandera TRUE o FALSE para la petición 
|title|String|Señala un mensaje de petición exitosa o error cuando suceda
|message|String|mensaje para conocer el estatus de la petición|
| **Encabezado**|||
|claveOtorgante|String|La **clave del otorgante** con la que se afilio a circulo de crédito.
|nombreOtorgante|String|Es el nombre corto con el que Circulo de Crédito denomina y entrega al Usuario que reporta la información.
|version|Integer|La **versión** Es el número de la versión donde el valor numérico es 4.
|formatoFecha|String|El **formato** de fecha que deberán tener todas las fechas proporcionadas por el otorgante ("dd/MM/yyyy", "MM/dd/yyyy", "yyyy/MM/dd").
|**Datos Generales**||||
|apellidoPaterno|String|El **Apellido Paterno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|apellidoMaterno|String|El **Apellido Materno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|apellidoAdicional|String|El Apellido de casada que pudiera tener una persona física.
|nombres|String|El nombre completo del Usuario (primer nombre, segundo nombre y/o nombres compuestos). No usar abreviaturas, iniciales, acentos y/o puntos.
|fechaNacimiento|String|La fecha de nacimiento del cliente reportado.
|rfc|String|El RFC del cliente el cual debe constar de 4 posiciones alfabéticas las siguientes 2 deben contener el numero correspondiente aL año de nacimiento del cliente los 4 siguientes corresponden 2 al mes y los otros 2 al día de la fecha de nacimiento. Los últimos 3 opcionales corresponden a la homoclave del cliente.
|curp|String|La clave CURP del cliente.
|nss|String|El numero de seguridad social del cliente.
|cveNacionalidad|String|La clave de la Nacionalidad del Cliente.
|cveResidencia|Integer|La situación de la vivienda del Cliente 1 = Propietario, 2 = Renta, 3 = Vive con familiares, 4 = Vivienda hipotecada, 5 = No disponible.
|numeroLicenciaConducir|String|El número de la licencia de conducir del cliente.
|cveEstadoCivil|String|El estado civil del Cliente. D=Divorciado, L=Unión Libre, C=Casado, S=Soltero, V=Viudo, E=Separado.
|sexo|String|El estado civil en el que se encuentra el cliente F=Femenino, M=Masculino.
|cveElectorIFE|String|El numero de la identificación oficial del cliente.
|numeroDependientes|Integer|El número de personas que dependen economicamente del cliente.
|fechaDefuncion|String||La fecha de muerte del cliente (en caso de haber fallecido ya).
|tipoPersona|String|La palabra PF para referirse a personas físicas.
|indicardorDefuncion|String|La frase "si" o "no" para referir si el cliente ya ha fallecido.
**Domicilios**|||
|direccionPersona|String|La dirección del Usuario incluyendo nombre de la calle, número exterior y/o interior.
|coloniaPersona|String|La colonia a la cual pertenece la dirección del Usuario.
|municipioPersona|String|El municipio a la cual pertenece la dirección del Usuario.
|ciudadPersona|String|La ciudad a la cual pertenece la dirección del Usuario.
|cveEstadoPersona|String|La abreviatura correspondiente al estado de la república al que pertenece la dirección del cliente.
|cpPersona|Integer|El código postal del domicilio del cliente debe estar compuesto por 5 dígitos, En caso de una longitud de 4 dígitos completar con cero a la izquierda ('08564).
|fechaResidenciaPersona|String|La fecha desde que el cliente vive en dicho domicilio.
|telefonoPersona|Long|El número telefónico del cliente.
|cveDomicilioPersona|String|La clave del tipo domicilio del Cliente N=Negocio, O=Domicilio del Otorgante, C=Casa, P=Apartado Postal, E = Empleo
|cveAsentamientoPersona|Integer|valor numérico que comprende del 0 al 55 para especificar el tipo de asentamiento donde reside el cliente. 
|cveOrigenDomicilio|String|La clave del país donde se encuentra el domicilio del cliente.
||**Empleos**||||
|nombreEmpresa|String|El nombre o razón social de la empresa donde labora el cliente, en caso de no tener trabajo debe reportarse Trabajador Independiente, Estudiante, Labores de Hogar, Jubilado, Desempleado o Exempleado.
|direccionEmpleo|String|La dirección de la empresa donde labora el cliente
|coloniaEmpleo|String|La colonia de la empresa donde labora el cliente
|municipioEmpleo|String|El municipio de la empresa donde labora el cliente
|ciudadEmpleo|String|La ciudad de la empresa donde labora el cliente
|cveEstadoEmpleo|String|La clave correspondiente al estado de la república de la empresa donde labora el cliente.
|cpEmpleo|Integer|El código postal correspondiente al estado de la empresa para la cuál trabaja el empleado.
|telefonoEmpleo|Long|El teléfono del empleo del cliente
|extensionEmpleo|Integer|La extensión del empleo del cliente.
|faxEmpleo|Long|El número de fax del empleo del empleado.
|puestoPersona|String|El puesto que desempeña el cliente.
|fechaContratacion|String|La fecha en la que fue contratado el cliente por su empleador.
|cveMoneda|String|Es el tipo de moneda que se le paga al Cliente en su empleo. peden ser MX=Pesos Mexicanos, US=Dólares, UD=Unidades de Inversión.
|salarioMensual|String|El salario mensual del cliente.
|fechaUltimoEmpleo|String|Fecha en la que el cliente laboró por última vez en la empresa.
|fechaVerificacionEmpleo|String|La fecha en al que se hizo la última verificación del empleo.
|cveOrigenEmpleo|String|La clave del país dónde se encuentra el domicilio del empleo del cliente.
|**Cuenta**||||
|cveActualOtorgante|String|La clave asignada por al círculo al otorgante.
|nombreOtorgante|String|El nombre del otorgante.
|cuentaActual|String|El número de cuenta.
|tipoResponsabilidad|String|La clave del tipo de responsabilidad I=Individual, M=Mancomunado, O=Obligatorio Solidario, A=Aval, T=Titular con Aval
|tipoCuenta|String|La clave del tipo de cuenta F=Pagos Fijos, H=Hipoteca, L=Sin Límite Preestablecido, R=Revolvente, Q=Quirografario, A=Crédito de Habilitación o Avío, E=Crédito Refaccionario, P=Crédito Prendario.
|tipoContrato|String|La clave que distingue la tipo de contrato que puede ser: AA=Arrendamiento Automotriz., HB=Hipotecario Bancario, AB=Automotriz Bancario, HE=Préstamo Tipo Home Equity, AE=Física Actividad Empresarial, HV=Hipotecario ó Vivienda, AM=Aparatos/Muebles, LC=Línea de Crédito, AR=Arrendamiento, MC=Mejoras a la Casa, AV=Aviación, NG=Préstamo No Garantizado, BC=Banca Comunal, PB=Préstamo Personal Bancario, BL=Bote/Lancha, PC=Procampo, BR=Bienes Raíces, PE=Préstamo Para Estudiante, CA=Compra De Automóvil, PG=Préstamo Garantizado, CC=Crédito Al Consumo, PQ=Préstamo Quirografario, CF=Crédito Fiscal, PM=Préstamo Empresarial, CO=Consolidación, PN=Préstamo de Nómina, PP=Préstamo Personal, ED=Editorial, SH=Segunda Hipoteca, EQ=Equipo, TC=Tarjeta De Crédito, FF=Fondeo Fira, TD=Tarjeta Departamental, FI=Fianza, TG=Tarjeta Garantizada, FT=Factoraje, TS=Tarjeta De Servicios, GS=Grupo Solidario, VR=Vehículo Recreativo, OT=Otros, NC=Desconocido
|cveUnidadMonetaria|String|La clave de la unidad monetaria para la cuenta MX = Pesos, US = Dólares, UD = Unidades de inversión
|valorActivoValuacion|Integer|no|El valor total del activo para propósitos de evaluación o recuperación.   
|numeroPagos|String|no|El numero de pagos efectuados por el cliente.
|frecuenciaPagos|String|La periodicidad con la que el cliente debe realizar sus pagos.
|montoPagar|Integer|El monto que el cliente debe pagar.
|fechaAperturaCuenta|String|La fecha en la que se aperturó la cuenta del cliente
|fechaUltimoPago|String|La fecha mas reciente en la que el cliente realizo un pago, se debe reportar la fecha 19010101 en el formato de fecha que halla seleccionado el usuario para sus fechas en este esquema.
|fechaUltimaCompra|String|La fecha en la que el cliente efectuó su última compra.
|fechaCierreCuenta|String|La fecha en la que se liquido o cerro un crédito.
|fechaCorte|String|La fecha del periodo que se esta reportando.
|garantia|String|El tipo de garantía para el crédito Otorgado.
|creditoMaximo|Integer|La cantidad máxima que dispone de crédito el cliente.
|saldoActual|Long|Es el importe total del adeudo que tiene el Clientes a la fecha de reporte incluyendo intereses.
|limiteCredito|Integer|El límite de crédito que el Usuario extiende al Cliente.
|saldoVencido|Integer|La cantidad generada a la fecha de reporte por atraso en pagos.
|numeroPagosVencidos|Integer|Es el número de pagos que NO ha efectuado el Cliente.
|pagoActual|String|El valor del “Pago Actual” es vigente, se debe reportar “V” (espacio”V”), para todos los demás casos, reportar de 01 a 84 pagos.
|historicoPagos|String|Reportar únicamente en el primer envió de la cuenta a Círculo de Crédito, quien posteriormente lo construirá basándose en la información reportada.
|cvePrevencion|String|La clave que describe la situación del crédito AD=Cuenta o monto en aclaración, CA=Cartera al Corriente Vendida o cedida a un usuario de una Sociedad, CC=Cuenta cancelada o cerrada, CD=Convenio y disminución de pago, CL=Cuenta cerrada que estuvo en cobranza y fue pagada totalmente sin causar quebranto, CO=Crédito en Controversia, CV=Cuenta que no está al corriente vendida o cedida a un usuario de una Sociedad, FD=Cuenta Fraudulenta, FN=Fraude no Atribuible al Consumidor, FP=Fianza pagada, FR=Adjudicación o aplicación de garantía, GP=Ejecución de Garantía Prendaria o Fiduciaria en Pago por Crédito, IA=Cuenta Inactiva, IM=Integrante causante de mora, IS=Integrante que fue subsidiado para evitar mora, LC=Convenio de finiquito o pago menor acordado con el Consumidor, LG=Pago menor por programa institucional o de gobierno, incluyendo los apoyos a damnificados por catástrofes naturales, LO=En Localización, LS=Tarjeta de Crédito Extraviada o Robada, NA=Cuenta al corriente vendida o cedida a un NO Usuario de una Sociedad, NV=Cuenta que no está al corriente vendida o cedida aunNO Usuario de una Sociedad, PC=Cuenta en Cobranza, PD=Prórroga otorgada debido a un desastre natural, PE=Prórroga otorgada al acreditado por situaciones especiales, PI=Prórroga otorgada al acreditado por invalidez, defunción, PR=Prórroga otorgada debido a una pérdida de relación laboral, RA=Cuenta reestructurada sin pago menor, por programa institucional o gubernamental, incluyendo los apoyos a damnificados por catástrofes naturales, RI=Robo de identidad, RF=Resolución judicial favorable al cliente, RN=Cuenta reestructurada debido a un proceso judicial, RV=Cuenta reestructurada sin pago menor por modificación de la situación del cliente, a petición de éste, SG=Demanda por el Otorgante, UP=Cuenta que causa quebranto, VR=Dación en Pagos ó Renta.
|totalPagosReportados|Integer|Es el total de pagos realizados por el Cliente a la fecha que se está reportando.
|cveAnteriorOtorgante|Long|Este elemento se hace requerido cuando existe un cambio en el Usuario.
|nombreAnteriorOtorgante|String|El campo aplica cuando por algún motivo se modifica el número de Usuario asignado por CC.
|numeroCuentaAnterior|String|El campo aplica cuando se efectúa una Reasignación de cuenta. Se captura una sola vez y en la siguiente entrega ya no se debe reportar el dato.
|fechaPrimerIncumplimiento|String|La fecha en que el consumidor incumplió por primera vez con algún pago. Una vez que ya se reportó una primera fecha ésta no debe modificarse. En caso de no contar con incumplimiento en el crédito deberá de reportar la fecha dummie 19010101 ó 19000101 en el formato seleccionado en el encabezado por el Usuario.
|saldoInsoluto|Long|Monto que se adeuda a la fecha de corte, no deberá incluir intereses, comisiones o cualquier otro accesorio. El Saldo Insoluto no puede ser mayor al Saldo Actual, pero sí podrán ser iguales.
|montoUltimoPago|Long|Es el pago parcial más reciente que el consumidor efectuó.	
|fechaIngresoCarteraVencida|String|La fecha en la que el acreditado fue promovido a cartera vencida, en el formato que se seleccionó en el encabezado.
|montoCorrespondienteIntereses|Integer|Importe correspondiente al monto de los intereses del crédito.
|formaPagoActualIntereses|String|El código que indica el comportamiento de pago de los intereses. El reporte de este pagó deberá mensualizarse (periodos de 30 días).
|diasVencimiento|Integer|El número de días que ha estado vencido el crédito, sí este se encuentra al corriente reportar cero 0.	
|plazoMeses|Integer|Es el plazo original del crédito se debe de reportar en días.
|montoCreditoOriginacion|Long|Es el Monto que se otorgó al Cliente en la apertura del crédito.
|correoElectronico|String|Ingresar la cuenta de correo electrónico del Cliente.
|descripcionerrores|String|descripción de los códigos de error
|codigosErrores|String|códigos de error 

#### Ejemplo Response - JSON

```bash
{
    "response": {
        "isSuccess": true,
        "success": {
            "title": "Petición exitosa",
            "message": "Se conecto de forma correcta"
        },
        "errors": [],
        "data": {
            "encabezado": {
                "claveOtorgante": "000579",
                "nombreOtorgante": "MULTIPLICATUNOMINA",
                "version": 4,
                "formatoFecha": "yyyy/MM/dd"
            },
            "datosGenerales": {
                "apellidoPaterno": "ABAD",
                "apellidoMaterno": "ESCALONA",
                "apellidoAdicional": null,
                "nombres": "PRUEBA",
                "fechaNacimiento": "1967/07/30",
                "rfc": "AAEA670730JL0",
                "curp": null,
                "nss": null,
                "cveNacionalidad": "MX",
                "cveResidencia": null,
                "numeroLicenciaConducir": null,
                "cveEstadoCivil": null,
                "sexo": null,
                "cveElectorIFE": null,
                "numeroDependientes": null,
                "fechaDefuncion": null,
                "tipoPersona": "PF",
                "indicardorDefuncion": null
            },
            "domicilios": [
                {
                    "direccionPersona": "AV MUNICIPAL SN",
                    "coloniaPersona": "CUILAPAN  DE GUERRERU",
                    "municipioPersona": "CUILAPAM  DE GUERRERO  OAX",
                    "ciudadPersona": "OAX",
                    "cveEstadoPersona": "OAX",
                    "cpPersona": 71240,
                    "fechaResidenciaPersona": null,
                    "telefonoPersona": null,
                    "cveDomicilioPersona": null,
                    "cveAsentamientoPersona": null,
                    "cveOrigenDomicilio": null
                }
            ],
            "empleos": [
                {
                    "nombreEmpresa": "INST ESTATAL EDUCACION PUBLICA OAXACA IEEPO",
                    "direccionEmpleo": "DOMICILIO",
                    "coloniaEmpleo": "SAN  PABLO",
                    "municipioEmpleo": "OAXACA DE JUAREZ",
                    "ciudadEmpleo": "OAX",
                    "cveEstadoEmpleo": "OAX",
                    "cpEmpleo": 69100,
                    "telefonoEmpleo": null,
                    "extensionEmpleo": null,
                    "faxEmpleo": null,
                    "puestoPersona": null,
                    "fechaContratacion": null,
                    "cveMoneda": null,
                    "salarioMensual": null,
                    "fechaUltimoEmpleo": null,
                    "fechaVerificacionEmpleo": null,
                    "cveOrigenEmpleo": "2"
                }
            ],
            "cuentas": [
                {
                    "cveActualOtorgante": "000422",
                    "nombreOtorgante": "MULTIPLICATUNOMINA",
                    "cuentaActual": "26015773-79121",
                    "tipoResponsabilidad": "I",
                    "tipoCuenta": "F",
                    "tipoContrato": "CP",
                    "cveUnidadMonetaria": "MX",
                    "valorActivoValuacion": null,
                    "numeroPagos": "96",
                    "frecuenciaPagos": "M",
                    "montoPagar": 817,
                    "fechaAperturaCuenta": "2017/02/21",
                    "fechaUltimoPago": "2018/03/21",
                    "fechaUltimaCompra": "2017/02/21",
                    "fechaCierreCuenta": "000010101",
                    "fechaCorte": "2018/04/24",
                    "garantia": null,
                    "creditoMaximo": 78441,
                    "saldoActual": 62916,
                    "limiteCredito": 78441,
                    "saldoVencido": 0,
                    "numeroPagosVencidos": null,
                    "pagoActual": "V",
                    "historicoPagos": null,
                    "cvePrevencion": null,
                    "totalPagosReportados": null,
                    "cveAnteriorOtorgante": null,
                    "nombreAnteriorOtorgante": null,
                    "numeroCuentaAnterior": null,
                    "fechaPrimerIncumplimiento": "1901/01/01",
                    "saldoInsoluto": 62916,
                    "montoUltimoPago": 15524,
                    "fechaIngresoCarteraVencida": null,
                    "montoCorrespondienteIntereses": null,
                    "formaPagoActualIntereses": null,
                    "diasVencimiento": 0,
                    "plazoMeses": 1460,
                    "montoCreditoOriginacion": 78441,
                    "correoElectronico": null
                }
            ],
            "descripcionerrores": null,
            "codigosErrores": null
        }
    }
}
```