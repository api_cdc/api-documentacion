﻿﻿# FicoScore

## Descripción

Consulta el fico score de una persona


## Solicitud simple rest

Se realiza la consulta mediante una solicitud simple **Rest**.

## URL
> `https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/fico`

## HTTP Method: POST

## Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|tipoScore|String|Si|**Tipo score**
|claveOtorgante|String|Si|**Clave del otorgante**
|folioOtorgante|String|Si|**Folio del otorgante**
|**persona**|Objeto|Si|Información de la persona
|└─primerNombre|String|Si|El **primer nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─segundoNombre|String|No|El **segundo nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─apellidoPaterno|String|Si|El **apellido paterno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoMaterno|String|No|El **apellido materno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellidoAdicional|String|No|Se debe utilizar para reportar el **apellido de casada**.
|└─fechaNacimiento|String|Si|**Fecha de nacimiento**
|└─rfc|String|No|Reglas:<br>Las primeras 4 posiciones deben ser alfabéticas.<br>1. 5 y 6 deben contener un número entre 00 y 99.<br>2. 7 y 8 deben contener un número entre 01 a 12.<br>3. 9 y 10 deben contener un número entre 01 a 31.  <br>4. 11 -13 homoclave (opcional).<br> Los RFC's de personas extranjeras deben cumplir con las características arriba mencionadas|
|└─direccion|String|Si|La **dirección de la persona** incluyendo nombre de la calle, número exterior y/o interior. Deben considerarse avenida, cerrada, manzana, lote, edificio, departamento etc.
|└─colonia|String|No|La **colonia** a la cual pertenece la dirección de la persona
|└─ciudad|String|No|La **Ciudad** a la cual pertenece la dirección de la persona
|└─codigoPostal|String|Si|El **código postal** reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado.  <br>En caso de una longitud de 4 dígitos completar con apóstrofe y cero a la izquierda ('08564)
|└─municipio|String|No|La **delegación** a la pertenece la dirección de la persona
|└─estado|String|Si|La abreviatura correspondiente
|└─pais|String|Si|Debe contener el país donde se encuentra el domicilio del Acreditado
|└─curp|String|No|Reglas: <br>  1\. 4 posiciones deben ser alfabéticas.  <br>2\. 5 y 6 posiciones deben contener un número entre 00 y 99 (año).<br>3\. 7 y 8 posiciones deben contener un número entre 01 y 12 (mes).<br>4\o.\+.- 9 y 10 posiciones deben contener un número entre 01 y 31 (día).  <br>5\. 11-16 posiciones deben ser alfabéticas.  <br>6\. 17 numérico (homoclave).  <br>7\. 18 numérico (Dígito Verificador).

#### Ejemplo Request - JSON

```bash
curl -X POST \
  'https://cabj2nd6x2.execute-api.us-west-1.amazonaws.com/beta-i/mx/v1/fico' \
   -H 'Authorization: {{TOKEN}}' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: {{API_KEY}}' \
  -d '{
	"tipoScore":{{tipoScore}},
    "claveOtorgante":"{{claveOtorgante}}",
	"folioOtorgante":"{{folioOtorgante}}",
	"persona":{
		"apellidoAdicional":	"{{apellidoAdicional}}" ,
		"apellidoMaterno":	"{{apellidoMaterno}}" ,
		"apellidoPaterno":	"{{apellidoPaterno}}" ,
		"ciudad":	"{{ciudad}}" ,
		"codigoPostal":	{{codigoPostal}},	
		"colonia":	"{{colonia}}" ,
		"curp":	"{{curp}}" ,
		"municipio":	"{{municipio}}" ,
		"direccion":	"{{direccion}}" ,
		"estado":	"{{estado}}" ,
		"folioOtorgante":	"{{folioOtorgante}}" ,
		"fechaNacimiento":	"{{fechaNacimiento}}" ,
		"pais":	"{{pais}}" ,
		"primerNombre":	"{{primerNombre}}" ,
		"rfc":	"{{rfc}}" ,
		"segundoNombre":	"{{segundoNombre}}" ,
		"usuario":	"{{usuario}}" 
	}
}'
```

  **Nota:** Se reemplazan las "{{value}}" por el valor correspondiente

## Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|**score**|int| Score
|**exclusionCode**|int| Exclusion Code
|**erroCode**|int| Error Code
|**razones**|array| 
|└─clave|String|Clave
|└─descripcion|String|Descripción

## Ejemplo Response - JSON

```bash
{
    "response": {
        "isSuccess": true,
        "success": null,
        "errors": [],
        "data": {
            "score": 0,
            "exclusionCode": 0,
            "erroCode": 0,
            "razones": [
                {
                    "clave": "D8",
                    "descripcion": "Morosidad grave, registro público adverso o cobranza reportada."
                },
                {
                    "clave": "D2",
                    "descripcion": "Morosidad en cuentas."
                },
                {
                    "clave": "E4",
                    "descripcion": "Falta de información reciente de la cuenta"
                },
                {
                    "clave": "E0",
                    "descripcion": "Información Demográfica."
                }
            ]
        }
    }
}
```